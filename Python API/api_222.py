#########################################################################################################################
# v2.0.0 : 03-Jan-2023 Adi : Tambah fungsi Generate Matrix Customer (issue screening lambet 11 ribu watchlist vs 8 juta customer)
# 							 Tambah Screening by Customer Matrix (Flag ON/OF di AML_GLOBAL_PARAMETER ID 11)
# v2.1.0 : 11-Jan-2023 Adi : Tambah Screening Ejaan Lama
#							 Tambah End Point untuk Case Management AI
# v2.1.1 : 27-Jan-2023 Adi : Comment screening ejaan lama di function Screening(Resource) ada complain dari team LC FAMA lambat untuk screening batch
# v2.1.2 : 03-Feb-2023 Adi : Ubah load watchlist data menjadi on the fly sesuai match result. Karena kalau load di awal untuk 16 juta data wasting memory
# v2.1.3 : 21-Feb-2023 Adi : Truncate Table AML_WATCHLIST_MATRIX instead Delete in GenerateMatrix to reduce log DB usage
#							 Re-Call load_matrix_aml() in the end of GenerateMatrix.
# v2.1.4 : 28-Feb-2023 Adi : Tambah handler jika sedang process Generate Watchlist Screening di mana table AML_WATCHLIST_SCREENING di-truncate dan diisi kembali
# v2.1.5 : 08-Mar-2023 Adi : Case Management Prediction tambah generate model (.pkl) jika filenya belum ada.
# v2.1.6 : 27-Mar-2023 Adi : Delete matched results ID that are not exists in Watchlist Database anymore.
# v2.1.7 : 13-Apr-2023 Adi : Tambah request argument isTransactionScreening untuk screening API. Jika nilainya 1 ambil bobot dari AML_SCREENING_TRANSACTION_MATCH_PARAMETER
#							 Tambah juga parameter _minimumSimaliry untuk membedakan screening onboarding dengan transaction saat matching
# v2.1.8 : 09-May-2023 Adi : Tambah Log untuk screening API untuk cek performance
# 							 Get only TOP x matched results. Case more than 1000 rows matched screening API timeout (more than 18 seconds)
# v2.1.9 : 17-May-2023 Adi : Tambah Try...Except di function GenerateMatrix
# v2.2.0 : 19-May-2023 Adi : Ubah Case Management AI Prediction dari SVM ke Logistic Regression 
# v2.2.1 : 14-Jun-2023 Adi : Tambah Instances Number Successfully Loaded for Load Balancer
# v2.2.2 : 13-Sep-2023 Adi : Using Singleton Pattern
#							 Add delete variables to free up memory
#                            Using cursor inside "with statement"
#                            Move logic top X to the final results
#							 Remove ON/OFF on Generate Matix Customer (isUSeCustomerAsMatrix), because it's a must for Screening Delta.
#							 Remove Double Jobs Load Matrix SIPENDAR
# #######################################################################################################################
# NEXT : Split api.py into many .py for more easy maintenance
#########################################################################################################################

strVersion = "2.2.2 (13-Sep-2023)"
print("Python API version " + strVersion)

import warnings
warnings.filterwarnings("ignore")

from flask import Flask
from flask_restful import Resource, Api, reqparse

app = Flask(__name__)
api = Api(app)

#import general libraries
import os
import json
import time
import uuid
import urllib

#import DB connector libraries
import pyodbc
from sqlalchemy import create_engine

#import libraries for encrypt/decrypt
from cryptography.fernet import Fernet

#import libraries used for machine learning
import pandas as pd
import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse import isspmatrix_csr
import sparse_dot_topn.sparse_dot_topn as ct
import sparse_dot_topn.sparse_dot_topn_threaded as ct_thread
from sklearn.feature_extraction.text import TfidfVectorizer
import pickle

#get time awal load API
t_awal = time.time()

##### 28-Nov-2022 Adi : Tambah variable InProgressRefreshMatrix
#Untuk menandakan di response message "In Progress Refresh Matrix" agar .net API melakukan retry
#Sesuai Application Parameter 8015
InProgressRefreshMatrix_AML = 0

#14-Sep-2023 Adi : Create Singleton Class for global static parameters
#the singleton pattern is a pattern where only a single instance of a class is instantiated and used throughout the designed system
class SingletonParam:
	def __init__(self):
		##### Application Version (Put 1.0 as initial)
		self.version = "1.0.0"

		##### Load Connection String from api.config
		dir_path = os.path.dirname(os.path.realpath(__file__))

		# Function to load the encrypt/decrypt key for connection string
		def call_key():
			return open("pass.key", "rb").read()
			
		with open(dir_path + '/api.config',) as infile:
			config = json.load(infile)
			infile.close()

		key = call_key()
		string = config["connectionString"].encode()
		encryptor = Fernet(key)
		decoded_string = encryptor.decrypt(string)
		self.connString = decoded_string.decode()

		del dir_path, config, key, string, encryptor, decoded_string

		##### Load Global Static Params into Singleton
		with pyodbc.connect(self.connString) as _conn:
			with _conn.cursor() as _cursor:
				# Maximum core used for matching
				_cursor.execute("SELECT SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID = 8005")
				self.maximumCore = int(_cursor.fetchval())

				# Maximum Row Match taken
				_cursor.execute("SELECT SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID = 8006")
				self.maximumRowMatch = int(_cursor.fetchval())

				# Minimum Similarity taken from result
				_cursor.execute("SELECT SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID = 8007")
				self.minimumSimilarity = float(_cursor.fetchval()) / 100

				# Port for Python Serve
				_cursor.execute("SELECT SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID = 8010")
				self.portPython = int(_cursor.fetchval())

				# Number of Rows per matrix
				_cursor.execute("SELECT SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID = 8013")
				self.pageSize = int(_cursor.fetchval())


		##### Load initial Global Dynamic Params into Singleton
		#self.load_match_score_parameter()
		
	def load_match_score_parameter(self):
		with pyodbc.connect(self.connString) as _conn:
			with _conn.cursor() as _cursor:
				# Match Score Parameter
				_cursor.execute("SELECT TOP 1 * FROM AML_SCREENING_MATCH_PARAMETER")
				arr_matchscore = _cursor.fetchone()
				self.nameWeight = float(arr_matchscore[2]) / 100
				self.dobWeight = float(arr_matchscore[3]) / 100
				self.nationalityWeight = float(arr_matchscore[4]) / 100
				self.identityWeight = float(arr_matchscore[5]) / 100
				self.minReturnPercentage = float(arr_matchscore[1]) / 100

				del arr_matchscore

	def load_match_score_trx_parameter(self):
		with pyodbc.connect(self.connString) as _conn:
			with _conn.cursor() as _cursor:
				_cursor.execute("SELECT TOP 1 * FROM AML_SCREENING_TRANSACTION_MATCH_PARAMETER")

				arr_matchscore = _cursor.fetchone()
				self.nameWeightTrx = float(arr_matchscore[2]) / 100
				self.dobWeightTrx = float(arr_matchscore[3]) / 100
				self.nationalityWeightTrx = float(arr_matchscore[4]) / 100
				self.identityWeightTrx = float(arr_matchscore[5]) / 100
				self.minReturnPercentageTrx = float(arr_matchscore[1]) / 100

				del arr_matchscore

	def load_parameter_top_matched(self):
		with pyodbc.connect(self.connString) as _conn:
			with _conn.cursor() as _cursor:
				_cursor.execute("SELECT ParameterValue FROM AML_Global_Parameter WHERE PK_GlobalReportParameter_ID = 8")
				self.intTopMatched = int(_cursor.fetchval())

	def load_parameter_write_log(self):
		with pyodbc.connect(self.connString) as _conn:
			with _conn.cursor() as _cursor:
				_cursor.execute("SELECT ParameterValue FROM AML_Global_Parameter WHERE PK_GlobalReportParameter_ID = 9")
				self.isWriteLog = int(_cursor.fetchval())




#Starting python service
print("Python API Service is starting...")
t1 = time.time()

# v2.1.8 : 05-May-2023 Adi : Tambah Log untuk screening API untuk cek performance
# Tambah function untuk write log ke EODLogSP
def WriteLog(requestID, logMessage):
	singleton_param = SingletonParam()
	singleton_param.load_parameter_write_log()

	if singleton_param.isWriteLog == 1:
		with pyodbc.connect(singleton_param.connString) as _conn:
			with _conn.cursor() as _cursor:
				_cursor.execute("INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES (GETDATE(), ?, ?)", (requestID, logMessage))

	del singleton_param

# Function Cosine Similarity
def awesome_cossim_top(A, B, ntop, lower_bound=0, use_threads=False, n_jobs=1):
	if not isspmatrix_csr(A):
		A = A.tocsr()

	if not isspmatrix_csr(B):
		B = B.tocsr()

	M, K1 = A.shape
	K2, N = B.shape

	idx_dtype = np.int32

	nnz_max = M * ntop
	
	if len(A.indices) == 0 or len(B.indices) == 0:
		indptr = np.zeros(M + 1, dtype=idx_dtype)
		indices = np.zeros(nnz_max, dtype=idx_dtype)
		data = np.zeros(nnz_max, dtype=A.dtype)
		return csr_matrix((data, indices, indptr), shape=(M, N))

	indptr = np.empty(M+1, dtype=idx_dtype)
	indices = np.empty(nnz_max, dtype=idx_dtype)
	data = np.empty(nnz_max, dtype=A.dtype)

	if not use_threads:

		ct.sparse_dot_topn(
			M, N, np.asarray(A.indptr, dtype=idx_dtype),
			np.asarray(A.indices, dtype=idx_dtype),
			A.data,
			np.asarray(B.indptr, dtype=idx_dtype),
			np.asarray(B.indices, dtype=idx_dtype),
			B.data,
			ntop,
			lower_bound,
			indptr, indices, data)

	else:
		if n_jobs < 1:
			err_str = "You select the multi-thread mode and n_job must be a value greater equal than 1!"
			raise ValueError(err_str)

		ct_thread.sparse_dot_topn_threaded(
			M, N, np.asarray(A.indptr, dtype=idx_dtype),
			np.asarray(A.indices, dtype=idx_dtype),
			A.data,
			np.asarray(B.indptr, dtype=idx_dtype),
			np.asarray(B.indices, dtype=idx_dtype),
			B.data,
			ntop,
			lower_bound,
			indptr, indices, data, n_jobs)

	return csr_matrix((data, indices, indptr), shape=(M, N))

# 15-Nov-2022 Adi : Define function for loading AML Watchlist Matrix so it can called from Load Balancer
def load_matrix_aml():
	print("Load AML watchlist matrix...")

	# Flag Load Matrix in Progress
	global InProgressRefreshMatrix_AML
	InProgressRefreshMatrix_AML = 1	

	global tf_idf_matrix_name
	global vectorizer_name
	global WatchlistListID_name

	global tf_idf_matrix_nationality
	global vectorizer_nationality
	global WatchlistListID_nationality

	global tf_idf_matrix_identity_number
	global vectorizer_identity_number
	global WatchlistListID_identity_number

	tf_idf_matrix_name = []
	WatchlistListID_name = []

	tf_idf_matrix_nationality = []
	WatchlistListID_nationality = []

	tf_idf_matrix_identity_number = []
	WatchlistListID_identity_number = []	

	singleton_param = SingletonParam()
	with pyodbc.connect(singleton_param.connString) as _conn:
		with _conn.cursor() as _cursor:
			# Load Matrix NAME
			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM AML_WATCHLIST_MATRIX WHERE MATRIX_NAME LIKE 'Watchlist_Name_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				tempMatrix = pickle.loads(row[0])
				if not isspmatrix_csr(tempMatrix):
					tempMatrix = tempMatrix.tocsr()
				tf_idf_matrix_name.append(tempMatrix)
				WatchlistListID_name.append(pickle.loads(row[1]))
				if(len(tf_idf_matrix_name) == 1):
					vectorizer_name = pickle.loads(row[2])

				del tempMatrix

			# Load Matrix NATIONALITY
			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM AML_WATCHLIST_MATRIX WHERE MATRIX_NAME LIKE 'Watchlist_Nationality_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				tempMatrix = pickle.loads(row[0])
				if not isspmatrix_csr(tempMatrix):
					tempMatrix = tempMatrix.tocsr()
				tf_idf_matrix_nationality.append(tempMatrix)
				WatchlistListID_nationality.append(pickle.loads(row[1]))
				if(len(tf_idf_matrix_nationality) == 1):
					vectorizer_nationality = pickle.loads(row[2])

				del tempMatrix

			# Load Matrix IDENTITY
			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM AML_WATCHLIST_MATRIX WHERE MATRIX_NAME LIKE 'Watchlist_Identity_Number_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				tempMatrix = pickle.loads(row[0])
				if not isspmatrix_csr(tempMatrix):
					tempMatrix = tempMatrix.tocsr()
				tf_idf_matrix_identity_number.append(tempMatrix)
				WatchlistListID_identity_number.append(pickle.loads(row[1]))
				if(len(tf_idf_matrix_identity_number) == 1):
					vectorizer_identity_number = pickle.loads(row[2])

				del tempMatrix

	# Flag Load Matrix is Done
	InProgressRefreshMatrix_AML = 0	

	del singleton_param

# 4-Jan-2023 Adi : Tambah load Matrix AML Customer
def load_matrix_aml_customer():
	print("Load AML Customer matrix...")

	# Flag Load Matrix in Progress
	global InProgressRefreshMatrix_AML
	InProgressRefreshMatrix_AML = 1	

	global customer_tf_idf_matrix_name
	global customer_vectorizer_name
	global customer_WatchlistListID_name

	global customer_tf_idf_matrix_nationality
	global customer_vectorizer_nationality
	global customer_WatchlistListID_nationality

	global customer_tf_idf_matrix_identity_number
	global customer_vectorizer_identity_number
	global customer_WatchlistListID_identity_number

	customer_tf_idf_matrix_name = []
	customer_WatchlistListID_name = []

	customer_tf_idf_matrix_nationality = []
	customer_WatchlistListID_nationality = []

	customer_tf_idf_matrix_identity_number = []
	customer_WatchlistListID_identity_number = []	

	singleton_param = SingletonParam()
	with pyodbc.connect(singleton_param.connString) as _conn:
		with _conn.cursor() as _cursor:
			# Load Matrix NAME
			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM AML_WATCHLIST_MATRIX WHERE MATRIX_NAME LIKE 'CUSTOMER_Name_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				tempMatrix = pickle.loads(row[0])
				if not isspmatrix_csr(tempMatrix):
					tempMatrix = tempMatrix.tocsr()
				customer_tf_idf_matrix_name.append(tempMatrix)
				customer_WatchlistListID_name.append(pickle.loads(row[1]))
				if(len(customer_tf_idf_matrix_name) == 1):
					customer_vectorizer_name = pickle.loads(row[2])

				del tempMatrix

			# Load Matrix NATIONALITY
			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM AML_WATCHLIST_MATRIX WHERE MATRIX_NAME LIKE 'CUSTOMER_Nationality_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				tempMatrix = pickle.loads(row[0])
				if not isspmatrix_csr(tempMatrix):
					tempMatrix = tempMatrix.tocsr()
				customer_tf_idf_matrix_nationality.append(tempMatrix)
				customer_WatchlistListID_nationality.append(pickle.loads(row[1]))
				if(len(customer_tf_idf_matrix_nationality) == 1):
					customer_vectorizer_nationality = pickle.loads(row[2])

				del tempMatrix

			# Load Matrix IDENTITy
			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM AML_WATCHLIST_MATRIX WHERE MATRIX_NAME LIKE 'CUSTOMER_Identity_Number_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				tempMatrix = pickle.loads(row[0])
				if not isspmatrix_csr(tempMatrix):
					tempMatrix = tempMatrix.tocsr()
				customer_tf_idf_matrix_identity_number.append(tempMatrix)
				customer_WatchlistListID_identity_number.append(pickle.loads(row[1]))
				if(len(customer_tf_idf_matrix_identity_number) == 1):
					customer_vectorizer_identity_number = pickle.loads(row[2])

				del tempMatrix

	# Flag Load Matrix is Done
	InProgressRefreshMatrix_AML = 0

	del singleton_param

#Function for loading watchlist to memory
def load_watchlist_data():
	print("Load watchlist data starting...")
	t1 = time.time()

	global df_watchlist_nationality

	singleton_param = SingletonParam()
	with pyodbc.connect(singleton_param.connString) as _conn:
		df_watchlist_nationality = pd.read_sql(
			"SELECT [PK] AS ID\
			,LEFT([NATIONALITY],500) AS NATIONALITY_WATCHLIST\
		FROM [AML_WATCHLIST_NATIONALITY]",
		_conn,
		index_col='ID')

	del singleton_param

	t = time.time() - t1
	print("Load watchlist data finished in " + str(t) + " seconds.")

# 03-Feb-2023 Adi : Ubah load watchlist data on the fly sesuai match results
def load_watchlist_data_otf(strFilter):
	print("Load hit watchlist data starting...")
	t1 = time.time()

	singleton_param = SingletonParam()
	df_result = pd.DataFrame()

	with pyodbc.connect(singleton_param.connString) as _conn:
		InProgressGenerateWatchlistScreening = int(0)
		df_param = 	pd.read_sql('SELECT ParameterValue FROM AML_Global_Parameter WHERE PK_GlobalReportParameter_ID=16', _conn)
		if not df_param.empty:
			InProgressGenerateWatchlistScreening = int(df_param['ParameterValue'].iloc[0])

		strTableName = 'AML_WATCHLIST_SCREENING'
		if InProgressGenerateWatchlistScreening == 1 :
			strTableName = 'AML_WATCHLIST_SCREENING_TRANSIENT'

		df_result = pd.read_sql(
			'SELECT a.[PK_AML_WATCHLIST_SCREENING_ID] AS ID\
			,a.[ALIAS_NAME] AS NAME_WATCHLIST\
			,IIF(a.[DATE_OF_BIRTH] IS NULL, NULL, CONVERT(VARCHAR(10),a.[DATE_OF_BIRTH],126)) AS DOB_WATCHLIST\
			,a.[YOB] AS YOB_WATCHLIST\
			,a.[NATIONALITY] AS NATIONALITY_WATCHLIST\
			,a.[IDENTITYNUMBER] AS IDENTITY_NUMBER_WATCHLIST\
			,b.[CATEGORY_NAME]\
			,c.[TYPE_NAME]\
			,a.[PK_AML_WATCHLIST_SCREENING_ID] AS WL_ID\
			,a.[PK_AML_WATCHLIST_SCREENING_ID]\
			FROM ' + strTableName + ' a WITH (NOLOCK)\
			LEFT JOIN [AML_WATCHLIST_CATEGORY] b WITH (NOLOCK) ON a.FK_AML_WATCHLIST_CATEGORY_ID = b.PK_AML_WATCHLIST_CATEGORY_ID\
			LEFT JOIN [AML_WATCHLIST_TYPE] c WITH (NOLOCK) ON a.FK_AML_WATCHLIST_TYPE_ID = c.PK_AML_WATCHLIST_TYPE_ID\
			WHERE a.[PK_AML_WATCHLIST_SCREENING_ID] IN (' + strFilter + ')',
		_conn)

	t = time.time() - t1
	print("Load hit watchlist data finished in " + str(t) + " seconds.")	

	del singleton_param, df_param, InProgressGenerateWatchlistScreening, strTableName

	return df_result

def get_matches_df(uniqID, sparse_matrix, A, B, offset):
	non_zeros = sparse_matrix.nonzero()
    
	sparserows = non_zeros[0]
	sparsecols = non_zeros[1]
    
	nr_matches = sparsecols.size
    
	result_code = np.empty([nr_matches], dtype="S36")
	left_side_index = np.empty([nr_matches], dtype=int)
	right_side_index = np.empty([nr_matches], dtype=int)
	similairity = np.zeros(nr_matches)

	for index in range(0, nr_matches):
		result_code[index] = uniqID
		left_side_index[index] = A[int(sparserows[index])]
		right_side_index[index] = B[int(sparsecols[index]) + offset]
		similairity[index] = sparse_matrix.data[index]

	return pd.DataFrame({ "RESULT_CODE": result_code,
							"LEFT_SIDE_INDEX": left_side_index,
							"RIGHT_SIDE_INDEX": right_side_index,
							"SIMILARITY": similairity})

class Compare():
	def DoCompare(tf_idf_matrix, matrix, listID, fieldToCompare, uniqID, names, offset):
		t1 = time.time()

		singleton_param = SingletonParam()
		matches = awesome_cossim_top(tf_idf_matrix, matrix, singleton_param.maximumRowMatch, singleton_param.minimumSimilarity, True, singleton_param.maximumCore)

		t = time.time()-t1
		print("COMPARE: " + fieldToCompare + " " + str(t))
		t1 = time.time()

		matches_df = get_matches_df(uniqID, matches, names, listID, offset)

		t = time.time()-t1
		print("MATCHING: " + fieldToCompare + " " + str(t))
		t1 = time.time()

		engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" % urllib.parse.quote_plus(singleton_param.connString), fast_executemany=True)
		matches_df.to_sql('AML_SCREENING_RESULT_API', con=engine, if_exists='append', index=False)
		
		t = time.time()-t1
		print("INSERT DB: " + fieldToCompare + " " + str(t))
		t1 = time.time()

		del singleton_param, matches, matches_df

class Screening(Resource):
	def get(self):
		parser = reqparse.RequestParser()  # initialize
		parser.add_argument('requestid', type=str)
		parser.add_argument('fieldtocompare', type=str)
		args = parser.parse_args()  # parse arguments to dictionary

		uniqID = str(uuid.uuid4())
		requestid = args['requestid']
		fieldToCompare = args['fieldtocompare'].upper()

		t1 = time.time()
		singleton_param = SingletonParam()

		with pyodbc.connect(singleton_param.connString) as _conn:
			names = pd.read_sql_query("SELECT PK_AML_SCREENING_REQUEST_ID, " + fieldToCompare + " FROM AML_SCREENING_REQUEST WHERE REQUEST_ID = '" + requestid + "' AND ISNULL(" + fieldToCompare + ", '') <> '' ORDER BY PK_AML_SCREENING_REQUEST_ID", _conn)

		t = time.time()-t1
		print("SELECT SCREENING: " + fieldToCompare + " " + str(t))
		t1 = time.time()

		if not names.empty:
			if fieldToCompare.upper() == "NAME":
				matrix = tf_idf_matrix_name
				vectorizer = vectorizer_name
				listID = WatchlistListID_name
			elif fieldToCompare.upper() == "NATIONALITY":
				matrix = tf_idf_matrix_nationality
				vectorizer = vectorizer_nationality
				listID = WatchlistListID_nationality
			elif fieldToCompare.upper() == "IDENTITY_NUMBER":
				matrix = tf_idf_matrix_identity_number
				vectorizer = vectorizer_identity_number
				listID = WatchlistListID_identity_number

			org_names = names[fieldToCompare]
			tf_idf_matrix = vectorizer.transform(org_names)

			t = time.time()-t1
			print("CREATE MATRIX: " + fieldToCompare + " " + str(t))
			t1 = time.time()

			for i in range(len(matrix)):
				Compare.DoCompare(tf_idf_matrix, matrix[i], listID[i], fieldToCompare, uniqID, names["PK_AML_SCREENING_REQUEST_ID"], i * singleton_param.pageSize)

			del matrix, vectorizer, listID, org_names, tf_idf_matrix

		t = time.time()-t1
		print("DONE SCREENING: " + fieldToCompare + " " + str(t))

		del args, singleton_param, names

		return uniqID, 200  # return data and 200 OK


#Function to get matches in data frame
def get_matches_df_API(uniqID, sparse_matrix, A, B, offset, requestID):
	non_zeros = sparse_matrix.nonzero()

	sparserows = non_zeros[0]
	sparsecols = non_zeros[1]

	nr_matches = sparsecols.size

	result_code = np.empty([nr_matches], dtype="S36")
	left_side_index = np.empty([nr_matches], dtype=int)
	right_side_index = np.empty([nr_matches], dtype=int)
	similairity = np.zeros(nr_matches)

	for index in range(0, nr_matches):
		result_code[index] = uniqID
		left_side_index[index] = A[int(sparserows[index])]
		right_side_index[index] = B[int(sparsecols[index]) + offset]
		similairity[index] = sparse_matrix.data[index]

	df_result = pd.DataFrame({"REQUEST_ID": requestID,
				"RESPONSE_ID": result_code,
				"LEFT_SIDE_INDEX": left_side_index,
				"RIGHT_SIDE_INDEX": right_side_index,
				"SIMILARITY": similairity})
	
	del non_zeros, sparserows, sparsecols, nr_matches, result_code, left_side_index, right_side_index, similairity

	return df_result

#Function to compare 
class CompareAPI():
	def DoCompareAPI(tf_idf_matrix, matrix, listID, fieldToCompare, uniqID, names, offset, requestID, _minimumSimilarity):
		t1 = time.time()

		singleton_param = SingletonParam()
		#matches = awesome_cossim_top(tf_idf_matrix, matrix, maximumRowMatch, minimumSimilarity, True, maximumCore)
		matches = awesome_cossim_top(tf_idf_matrix, matrix, singleton_param.maximumRowMatch, _minimumSimilarity, True, singleton_param.maximumCore)

		t = time.time()-t1
		print("COMPARE: " + fieldToCompare + " " + str(t))
		t1 = time.time()

		matches_df = get_matches_df_API(uniqID, matches, names, listID, offset, requestID)

		t = time.time()-t1
		print("MATCHING: " + fieldToCompare + " " + str(t))
		
		del singleton_param, matches, t, t1

		return matches_df

#Main function for data matching
class Matching_API():
	def DoMatching(pkRequestId, requestID, fieldToCompare, fieldValue, _minimumSimilarity):
		uniqID = str(uuid.uuid4())
		result_df = pd.DataFrame()

		singleton_param = SingletonParam()

		if not fieldValue == "":
			t1 = time.time()

			names = fieldValue.split(",")
			org_names = names

			if fieldToCompare.upper() == "NAME":
				matrix = tf_idf_matrix_name
				vectorizer = vectorizer_name
				listID = WatchlistListID_name
			elif fieldToCompare.upper() == "NATIONALITY":
				matrix = tf_idf_matrix_nationality
				vectorizer = vectorizer_nationality
				listID = WatchlistListID_nationality
			elif fieldToCompare.upper() == "IDENTITY_NUMBER":
				matrix = tf_idf_matrix_identity_number
				vectorizer = vectorizer_identity_number
				listID = WatchlistListID_identity_number

			tf_idf_matrix = vectorizer.transform(org_names)

			t = time.time()-t1
			print("CREATE MATRIX: " + fieldToCompare + " " + str(t))
			t1 = time.time()

			for i in range(len(matrix)):
				#matched_df = CompareAPI.DoCompareAPI(tf_idf_matrix, matrix[i], listID[i], fieldToCompare, uniqID, pkRequestId, i * pageSize, requestID)
				#13-Apr-2023 Adi 
				matched_df = CompareAPI.DoCompareAPI(tf_idf_matrix, matrix[i], listID[i], fieldToCompare, uniqID, pkRequestId, i * singleton_param.pageSize, requestID, _minimumSimilarity)
				result_df = pd.concat([result_df, matched_df]) 

			t = time.time()-t1
			print("DONE SCREENING: " + fieldToCompare + " " + str(t))

			del uniqID, names, org_names, matrix, vectorizer, listID, tf_idf_matrix, matched_df

		return result_df
	
class ScreeningAPI(Resource):
	def get(self):

		#String to Return
		obj = ""

		singleton_param = SingletonParam()

		if InProgressRefreshMatrix_AML == 0:				
			t1 = time.time()

			parser = reqparse.RequestParser()  # initialize
			parser.add_argument('pkrequestid', type=str)
			parser.add_argument('requestid', type=str)
			parser.add_argument('name', type=str)
			parser.add_argument('dob', type=str)
			parser.add_argument('nationality', type=str)
			parser.add_argument('identitynumber', type=str)

			#13-Apr-2023 Adi : tambah parameter isTransactionScreening
			parser.add_argument('istransaction', type=int)

			args = parser.parse_args()  # parse arguments to dictionary

			pkrequestid = args['pkrequestid']
			requestID = args['requestid']
			name = args['name']
			dob = args['dob']
			nationality = args['nationality']
			identitynumber = args['identitynumber'].upper()

			#13-Apr-2023 Adi : tambah parameter isTransactionScreening
			istransaction = args['istransaction']
			IsTransactionScreening = int(0)
			if not istransaction is None:
				IsTransactionScreening = istransaction

			#14-Sep-2023 Adi : Load match score parameter in case user changed the parameters, no need to restart python
			singleton_param.load_match_score_parameter()
			_nameWeight = singleton_param.nameWeight
			_dobWeight = singleton_param.dobWeight
			_nationalityWeight = singleton_param.nationalityWeight
			_identityWeight = singleton_param.identityWeight
			_minReturnPercentage = singleton_param.minReturnPercentage
			_minimumSimilarity = singleton_param.minimumSimilarity

			if IsTransactionScreening == 1:
				singleton_param.load_match_score_trx_parameter()
				_nameWeight = singleton_param.nameWeightTrx
				_dobWeight = singleton_param.dobWeightTrx
				_nationalityWeight = singleton_param.nationalityWeightTrx
				_identityWeight = singleton_param.identityWeightTrx
				_minReturnPercentage = singleton_param.minReturnPercentageTrx

			#13-Apr-2023 Adi : Jika minimum return percentage < minimum similarity, ambil yang minimum return percentage
			#Karena kalau misalnya screening match parameter disetting misalnya 50% tapi di app parameter 80%. 
			#Maka hasilnya gak akan dapat.
			if IsTransactionScreening == 1 or _minReturnPercentage < _minimumSimilarity:
				_minimumSimilarity = _minReturnPercentage

			WriteLog(requestID, "In Python : Matching Name, Nationality and Identity Number started")

			responseName = Matching_API.DoMatching(pkrequestid,requestID,"NAME",name,_minimumSimilarity)
			responseNationality = Matching_API.DoMatching(pkrequestid,requestID,"NATIONALITY",nationality,_minimumSimilarity)
			responseIdentity = Matching_API.DoMatching(pkrequestid,requestID,"IDENTITY_NUMBER",identitynumber,_minimumSimilarity)

			result_df = responseName			

			if not result_df.empty:
				result_df = result_df.reset_index(drop=True)

				#03-Feb-2023 Adi : Ubah load watchlist data menjadi on the fly sesuai match result.
				strFilter = ""
				for i in range(len(result_df)):
					selRightIndex = result_df.at[i,'RIGHT_SIDE_INDEX']
					strFilter = strFilter + str(selRightIndex) + ","

				#Remove comma in the end of string
				strFilter = strFilter[:-1]

				WriteLog(requestID, "In Python : Load Hit Watchlist Data on the fly started")

				if strFilter != "":
					df_watchlist_name_otf = load_watchlist_data_otf(strFilter)
				#End of 03-Feb-2023 Adi : Ubah load watchlist data menjadi on the fly sesuai match result.		

				#Rename the column
				result_df.columns = ['REQUEST_ID','RESPONSE_ID','PK_AML_SCREENING_REQUEST_ID','PK_AML_WATCHLIST_SCREENING_ID','MATCH_SCORE_NAME']
				result_df['WL_ID'] = result_df['PK_AML_WATCHLIST_SCREENING_ID']
				result_df['MATCH_SCORE_NAME'] = result_df['MATCH_SCORE_NAME'].apply(lambda x: x * 100)
				result_df['MATCH_SCORE_DOB'] = float(0)
				result_df['MATCH_SCORE_NATIONALITY'] = float(0)
				result_df['MATCH_SCORE_IDENTITY_NUMBER'] = float(0)
				result_df['MATCH_SCORE'] = float(0)

				#Fill Name,DOB,YOB,Nationality and Identity Number from url arguments
				result_df['NAME'] = name if name != "" else ""
				result_df['DOB'] = dob if dob != "" else ""
				result_df['YOB'] = dob[0:4] if dob != "" else ""
				result_df['NATIONALITY'] = nationality if nationality != "" else ""
				result_df['IDENTITY_NUMBER'] = identitynumber if identitynumber != "" else ""

				#Create Columns:
				result_df['NAME_WATCHLIST'] = ""
				result_df['DOB_WATCHLIST'] = ""
				result_df['YOB_WATCHLIST'] = ""
				result_df['NATIONALITY_WATCHLIST'] = ""
				result_df['IDENTITY_NUMBER_WATCHLIST'] = ""
				result_df['CATEGORY_NAME'] = ""
				result_df['TYPE_NAME'] = ""				

				#27-Mar-2023 Adi : Delete matched results ID that are not exists in Watchlist Database anymore  
				result_df_temp = result_df
				for i in range(len(result_df)):
					selRightIndex = result_df_temp.at[i,'PK_AML_WATCHLIST_SCREENING_ID']		
					if not df_watchlist_name_otf['WL_ID'].isin([selRightIndex]).any():
						#print('tidak ketemu ID ' + str(selRightIndex))
						result_df.drop(result_df[result_df['PK_AML_WATCHLIST_SCREENING_ID'] == selRightIndex].index, inplace = True)

				result_df = result_df.reset_index(drop=True)
				#End of 27-Mar-2023 Adi : Delete matched results ID that are not exists in Watchlist Database anymore 

				WriteLog(requestID, "In Python : Update Dataframe from Watchlist Data started")

				#print(result_df[['WL_ID','PK_AML_WATCHLIST_SCREENING_ID']])
				result_df.set_index('WL_ID', inplace=True)
				result_df.update(df_watchlist_name_otf.set_index('WL_ID'))
				result_df = result_df.reset_index(drop=True)

				WriteLog(requestID, "In Python : Match Score Calculation started")

				#Get Similarity data for Nationality
				for i in range(len(responseNationality)):
					selNationality = df_watchlist_nationality.iat[responseNationality.at[i,'RIGHT_SIDE_INDEX']-1,0]
					result_df.loc[result_df['NATIONALITY_WATCHLIST'] == selNationality, ['MATCH_SCORE_NATIONALITY']] = float(responseNationality.at[i,'SIMILARITY'])*100

				#Get Similarity data for Identity Number
				for i in range(len(responseIdentity)):
					selIdentityNumberIndex = responseIdentity.at[i,'RIGHT_SIDE_INDEX']
					result_df.loc[result_df['PK_AML_WATCHLIST_SCREENING_ID'] == selIdentityNumberIndex, ['MATCH_SCORE_IDENTITY_NUMBER']] = float(responseIdentity.at[i,'SIMILARITY'])*100

				#Get Similarity data for DOB and also get Total Match Score
				for i in range(len(result_df)):
					if result_df.at[i,'DOB'] == "":
						result_df.at[i,'MATCH_SCORE_DOB'] = 0
					elif result_df.at[i,'DOB'] == result_df.at[i,'DOB_WATCHLIST']:
						result_df.at[i,'MATCH_SCORE_DOB'] = 100
					elif result_df.at[i,'YOB'] == result_df.at[i,'YOB_WATCHLIST']:
						result_df.at[i,'MATCH_SCORE_DOB'] = 30

					totalScore = float(0)
					totalDivisor = float(0)

					if name != "":
						totalScore += result_df.at[i,'MATCH_SCORE_NAME'] * _nameWeight
						totalDivisor += _nameWeight

					if dob != "":
						totalScore += result_df.at[i,'MATCH_SCORE_DOB'] * _dobWeight
						totalDivisor += _dobWeight

					if nationality != "":
						totalScore += result_df.at[i,'MATCH_SCORE_NATIONALITY'] * _nationalityWeight
						totalDivisor += _nationalityWeight

					if identitynumber != "":
						totalScore += result_df.at[i,'MATCH_SCORE_IDENTITY_NUMBER'] * _identityWeight
						totalDivisor += _identityWeight

					if totalDivisor != 0:
						result_df.at[i,'MATCH_SCORE'] = (totalScore / totalDivisor)

				result_df.drop(result_df[result_df['MATCH_SCORE'] < (_minReturnPercentage*100)].index, inplace = True)

				# 9-May-2023 Adi : Get only TOP x matched results
				_intTopMatched = 10
				singleton_param.load_parameter_top_matched()
				_intTopMatched
				with pyodbc.connect(singleton_param.connString) as _conn:
					with _conn.cursor() as _cursor:
						_cursor.execute("SELECT TOP 1 * FROM AML_Global_Parameter WITH (NOLOCK) WHERE PK_GlobalReportParameter_ID=8")
						_intTopMatched = int(_cursor.fetchval())

				#13-Sep-2023 Adi : Move _intTopMatched to the final results
				if not result_df.empty:
					result_df = result_df.sort_values('MATCH_SCORE',ascending=False).head(_intTopMatched)
					result_df = result_df.reset_index(drop=True)

				t = time.time()-t1
				print("DONE SCREENING API " + requestID + " :" + str(t))

				result_df = result_df.replace(np.nan, '', regex=True)
				#responseAll = result_df.to_json(orient='records', indent=2)
				#self.write(responseAll)  # return data and 200 OK

			obj = ""

			if not result_df.empty:
				WriteLog(requestID, "In Python : Convert Dataframe result to JSON started")

				json_data = result_df.to_json(orient='records')
				obj = json.loads(json_data)
			else:
				obj = json.loads("[]")

		else :	#Jika in progress reload matrix
			obj = "In Progress Refresh Matrix"

		#13-Sep-2023 Adi : Release variables to free up memory
		import gc
		del IsTransactionScreening, _conn, _cursor, _dobWeight, _identityWeight, _intTopMatched, _minReturnPercentage, _minimumSimilarity, _nameWeight, _nationalityWeight, args, df_watchlist_name_otf, dob, i, identitynumber, istransaction, json_data, name, nationality, parser, pkrequestid, requestID, responseIdentity, responseName, responseNationality, result_df, result_df_temp, selRightIndex, strFilter, t, t1, totalDivisor, totalScore
		del singleton_param
		gc.collect()		

		return obj, 200
# End of 17-Oct-2022 Adi : Create new research for Screening API performance

# 16-Nov-2022 Adi : Define function for Generate Matrix so it can called from Load Balancer
def generate_matrix(fieldId, fieldIdAlias, fieldToCompare, fieldToCompareAlias, table):
	global tf_idf_matrix_name
	global vectorizer_name
	global WatchlistListID_name

	global tf_idf_matrix_nationality
	global vectorizer_nationality
	global WatchlistListID_nationality

	global tf_idf_matrix_identity_number
	global vectorizer_identity_number
	global WatchlistListID_identity_number

	t1 = time.time()

	singleton_param = SingletonParam()
	with pyodbc.connect(singleton_param.connString) as _conn:
		names = pd.read_sql_query("SELECT " + fieldId + " AS " + fieldIdAlias + ", " + fieldToCompare + " AS " + fieldToCompareAlias + " FROM " + table + " WHERE ISNULL(" + fieldToCompare + ", '') <> '' ORDER BY " + fieldIdAlias, _conn)

		t = time.time()-t1
		print("SELECT MATRIX DATA: " + fieldToCompareAlias + " " + str(t))
		t1 = time.time()

		if not names.empty:
			org_names = names[fieldToCompareAlias]

			vectorizer = TfidfVectorizer(min_df=1, analyzer="char_wb", ngram_range=(3,3))
			matrix = vectorizer.fit_transform(org_names)
			listID = names[fieldIdAlias]

			t = time.time()-t1
			print("GENERATE MATRIX: " + fieldToCompareAlias + " " + str(t))
			t1 = time.time()

			with _conn.cursor() as _cursor:
				_cursor.execute("DELETE AML_WATCHLIST_MATRIX WHERE MATRIX_NAME LIKE ?", "WATCHLIST_" + fieldToCompareAlias + "_%")

			t = time.time()-t1
			print("DELETE PREV DATA: " + fieldToCompareAlias + " " + str(t))
			t1 = time.time()

			dataSize = len(org_names)
			counter = 0
			counterIndex = 0

			vectorizerDump = pickle.dumps(vectorizer)

			if fieldToCompareAlias.upper() == "NAME":
				tf_idf_matrix_name = []
				WatchlistListID_name = []
				vectorizer_name = vectorizer
			elif fieldToCompareAlias.upper() == "NATIONALITY":
				tf_idf_matrix_nationality = []
				WatchlistListID_nationality = []
				vectorizer_nationality = vectorizer
			elif fieldToCompareAlias.upper() == "IDENTITY_NUMBER":
				tf_idf_matrix_identity_number = []
				WatchlistListID_identity_number = []
				vectorizer_identity_number = vectorizer

			while counter < dataSize:
				t = time.time()-t1
				print("Processing data : " + str(counter) + " / " + str(dataSize) + " " + str(t))
				t1 = time.time()

				matrixSliced = matrix[counter: counter + singleton_param.pageSize].transpose()
				listIDSliced = listID[counter: counter + singleton_param.pageSize]

				t = time.time()-t1
				print("Slice Data : " + str(counter) + " / " + str(dataSize) + " " + str(t))
				t1 = time.time()

				matrixDump = pickle.dumps(matrixSliced)
				listIDDump = pickle.dumps(listIDSliced)

				with _conn.cursor() as _cursor:
					_cursor.execute("INSERT INTO AML_WATCHLIST_MATRIX (MATRIX_NAME, MATRIX, VECTORIZER, LIST_ID) VALUES(?, ?, ?, ?)", ('WATCHLIST_' + fieldToCompareAlias + "_" + str(counterIndex + 1).zfill(2), pyodbc.Binary(matrixDump), pyodbc.Binary(vectorizerDump), pyodbc.Binary(listIDDump)))

				t = time.time()-t1
				print("Insert DB : " + str(counter) + " / " + str(dataSize) + " " + str(t))
				t1 = time.time()

				if fieldToCompareAlias.upper() == "NAME":
					if not isspmatrix_csr(matrixSliced):
						matrixSliced = matrixSliced.tocsr()
					tf_idf_matrix_name.append(matrixSliced)
					WatchlistListID_name.append(listIDSliced)
				elif fieldToCompareAlias.upper() == "NATIONALITY":
					if not isspmatrix_csr(matrixSliced):
						matrixSliced = matrixSliced.tocsr()
					tf_idf_matrix_nationality.append(matrixSliced)
					WatchlistListID_nationality.append(listIDSliced)
				elif fieldToCompareAlias.upper() == "IDENTITY_NUMBER":
					if not isspmatrix_csr(matrixSliced):
						matrixSliced = matrixSliced.tocsr()
					tf_idf_matrix_identity_number.append(matrixSliced)
					WatchlistListID_identity_number.append(listIDSliced)

				counter = counter + singleton_param.pageSize
				counterIndex = counterIndex + 1

				del matrixDump, listIDDump, matrixSliced, listIDSliced

			del org_names, vectorizer, matrix, listID, dataSize, counter, counterIndex, vectorizerDump

		del names
		
	del singleton_param

	t = time.time()-t1
	print("Done Creating Matrix : " + fieldToCompareAlias + " " + str(t))


# 3-Jan-2023 Adi : Generate Matrix Customer
def generate_matrix_customer(fieldId, fieldIdAlias, fieldToCompare, fieldToCompareAlias, table):
	global customer_tf_idf_matrix_name
	global customer_vectorizer_name
	global customer_WatchlistListID_name

	global customer_tf_idf_matrix_nationality
	global customer_vectorizer_nationality
	global customer_WatchlistListID_nationality

	global customer_tf_idf_matrix_identity_number
	global customer_vectorizer_identity_number
	global customer_WatchlistListID_identity_number

	t1 = time.time()

	singleton_param = SingletonParam()
	with pyodbc.connect(singleton_param.connString) as _conn:
		names = pd.read_sql_query("SELECT " + fieldId + " AS " + fieldIdAlias + ", " + fieldToCompare + " AS " + fieldToCompareAlias + " FROM " + table + " WHERE ISNULL(" + fieldToCompare + ", '') <> '' ORDER BY " + fieldIdAlias, _conn)

		t = time.time()-t1
		print("SELECT MATRIX DATA: " + fieldToCompareAlias + " " + str(t))
		t1 = time.time()

		if not names.empty:
			org_names = names[fieldToCompareAlias]

			vectorizer = TfidfVectorizer(min_df=1, analyzer="char_wb", ngram_range=(3,3))
			matrix = vectorizer.fit_transform(org_names)
			listID = names[fieldIdAlias]

			t = time.time()-t1
			print("GENERATE MATRIX: " + fieldToCompareAlias + " " + str(t))
			t1 = time.time()

			with _conn.cursor() as _cursor:
				_cursor.execute("DELETE AML_WATCHLIST_MATRIX WHERE MATRIX_NAME LIKE ?", "CUSTOMER_" + fieldToCompareAlias + "_%")

			t = time.time()-t1
			print("DELETE PREV DATA: " + fieldToCompareAlias + " " + str(t))
			t1 = time.time()

			dataSize = len(org_names)
			counter = 0
			counterIndex = 0

			vectorizerDump = pickle.dumps(vectorizer)

			if fieldToCompareAlias.upper() == "NAME":
				customer_tf_idf_matrix_name = []
				customer_WatchlistListID_name = []
				customer_vectorizer_name = vectorizer
			elif fieldToCompareAlias.upper() == "NATIONALITY":
				customer_tf_idf_matrix_nationality = []
				customer_WatchlistListID_nationality = []
				customer_vectorizer_nationality = vectorizer
			elif fieldToCompareAlias.upper() == "IDENTITY_NUMBER":
				customer_tf_idf_matrix_identity_number = []
				customer_WatchlistListID_identity_number = []
				customer_vectorizer_identity_number = vectorizer

			while counter < dataSize:
				t = time.time()-t1
				print("Processing data : " + str(counter) + " / " + str(dataSize) + " " + str(t))
				t1 = time.time()

				matrixSliced = matrix[counter: counter + singleton_param.pageSize].transpose()
				listIDSliced = listID[counter: counter + singleton_param.pageSize]

				t = time.time()-t1
				print("Slice Data : " + str(counter) + " / " + str(dataSize) + " " + str(t))
				t1 = time.time()

				matrixDump = pickle.dumps(matrixSliced)
				listIDDump = pickle.dumps(listIDSliced)

				with _conn.cursor() as _cursor:
					_cursor.execute("INSERT INTO AML_WATCHLIST_MATRIX (MATRIX_NAME, MATRIX, VECTORIZER, LIST_ID) VALUES(?, ?, ?, ?)", ('CUSTOMER_' + fieldToCompareAlias + "_" + str(counterIndex + 1).zfill(2), pyodbc.Binary(matrixDump), pyodbc.Binary(vectorizerDump), pyodbc.Binary(listIDDump)))

				t = time.time()-t1
				print("Insert DB : " + str(counter) + " / " + str(dataSize) + " " + str(t))
				t1 = time.time()

				if fieldToCompareAlias.upper() == "NAME":
					if not isspmatrix_csr(matrixSliced):
						matrixSliced = matrixSliced.tocsr()
					customer_tf_idf_matrix_name.append(matrixSliced)
					customer_WatchlistListID_name.append(listIDSliced)
				elif fieldToCompareAlias.upper() == "NATIONALITY":
					if not isspmatrix_csr(matrixSliced):
						matrixSliced = matrixSliced.tocsr()
					customer_tf_idf_matrix_nationality.append(matrixSliced)
					customer_WatchlistListID_nationality.append(listIDSliced)
				elif fieldToCompareAlias.upper() == "IDENTITY_NUMBER":
					if not isspmatrix_csr(matrixSliced):
						matrixSliced = matrixSliced.tocsr()
					customer_tf_idf_matrix_identity_number.append(matrixSliced)
					customer_WatchlistListID_identity_number.append(listIDSliced)

				counter = counter + singleton_param.pageSize
				counterIndex = counterIndex + 1
		
				del matrixDump, listIDDump, matrixSliced, listIDSliced

			del org_names, vectorizer, matrix, listID, dataSize, counter, counterIndex, vectorizerDump

		del names
		
	del singleton_param

	t = time.time()-t1
	print("Done Creating Matrix Customer : " + fieldToCompareAlias + " " + str(t))


class Matrix(Resource):
	def get(self):

		global InProgressRefreshMatrix_AML
		InProgressRefreshMatrix_AML = 1		

		parser = reqparse.RequestParser()  # initialize
		parser.add_argument('fieldid', type=str)
		parser.add_argument('fieldidalias', type=str)
		parser.add_argument('fieldtocompare', type=str)
		parser.add_argument('fieldtocomparealias', type=str)
		parser.add_argument('table', type=str)
		args = parser.parse_args()  # parse arguments to dictionary

		fieldId = args['fieldid'].upper()
		fieldIdAlias = args['fieldidalias'].upper()
		fieldToCompare = args['fieldtocompare'].upper()
		fieldToCompareAlias = args['fieldtocomparealias'].upper()
		table = args['table'].upper()

		generate_matrix(fieldId, fieldIdAlias, fieldToCompare, fieldToCompareAlias, table)

		InProgressRefreshMatrix_AML = 0

		del args, fieldId, fieldIdAlias, fieldToCompare, fieldToCompareAlias, table

		return "SUCCESS", 200  # return data and 200 OK


# 16-Nov-2022 Adi : Create new class for end point GenerateMatrix All (Name, Nationality, ID Number) with 1x Call
class GenerateMatrix(Resource):
	def get(self):
		t1 = time.time()
		singleton_param = SingletonParam()

		try:
			#21-Feb-2023 Adi : Truncate saja table Matrix nya agar tidak membebani Log DB
			with pyodbc.connect(singleton_param.connString) as _conn:
				with _conn.cursor() as _cursor:
					_cursor.execute("TRUNCATE TABLE AML_WATCHLIST_MATRIX")
			#End of 21-Feb-2023 Adi : Truncate saja table Matrix nya agar tidak membebani Log DB

			global InProgressRefreshMatrix_AML
			InProgressRefreshMatrix_AML = 1		

			global isUsedCustomerAsMatrix

			#1 Generate Matrix Name
			generate_matrix("PK_AML_WATCHLIST_SCREENING_ID", "PK_AML_WATCHLIST_SCREENING_ID", "ALIAS_NAME", "NAME", "AML_WATCHLIST_SCREENING")

			#2 Generate Matrix Nationality
			generate_matrix("PK", "PK", "Nationality", "Nationality", "AML_WATCHLIST_NATIONALITY")

			#3 Generate Matrix ID Number
			generate_matrix("PK_AML_WATCHLIST_SCREENING_ID", "PK_AML_WATCHLIST_SCREENING_ID", "IDENTITYNUMBER", "IDENTITY_NUMBER", "AML_WATCHLIST_SCREENING")

			#3-Jan-2023 Adi : CDD Screening Batch Using Customer as Matrix
			#13-Sep-2023 Adi : Remove ON/OFF logic generate Customer Matrix, because it is used for Screening Delta
			#if isUsedCustomerAsMatrix == 1:
			#1 Generate Matrix Name
			generate_matrix_customer("ROW_NO", "ROW_NO", "CUSTOMERNAME", "NAME", "vw_AML_CUSTOMER_FOR_SCREENING_New")

			#2 Generate Matrix Nationality
			generate_matrix_customer("ROW_NO", "ROW_NO", "NATIONALITY", "NATIONALITY", "vw_AML_CUSTOMER_FOR_SCREENING_New")

			#3 Generate Matrix ID Number
			generate_matrix_customer("ROW_NO", "ROW_NO", "IDENTITY_NUMBER", "IDENTITY_NUMBER", "vw_AML_CUSTOMER_FOR_SCREENING_New")

			#21-Feb-2023 Adi : Tambahin Load Matrix kembali karena ada error
			load_matrix_aml()

			#13-Sep-2023 Adi : Remove ON/OFF logic generate Customer Matrix, because it is used for Screening Delta
			# if isUsedCustomerAsMatrix == 1:
			load_matrix_aml_customer()		

			#08-Dec-2022 Adi : Tambahin LoadWatchlist juga
			load_watchlist_data()

			InProgressRefreshMatrix_AML = 0

			t = time.time()-t1
			print("Done Creating Overall Matrix in " + str(t) + " seconds")

			return "SUCCESS Refresh Matrix in " + str(t) + " seconds", 200  # return data and 200 OK	
		
		except Exception as error:
			t = time.time()-t1

			WriteLog(0, str(error))
			return "ERROR Refresh Matrix in " + str(t) + " seconds. Error : " + str(error), 500  # return 500 Error
		
		
################### 15-Nov-2022 Adi : Tambah End Point untuk Load Matrix ALL untuk dicall lewat Load Balancer
def load_matrix_all():
	load_matrix_aml()
	load_matrix_sipendar()
	load_matrix_sipendar_delta()
	load_matrix_sipendar_stakeholder()
	load_matrix_sipendar_stakeholder_delta()
	load_matrix_sipendar_prospect()

	# 4-Jan-2023 Adi : Tambah load Matrix AML Customer
	#13-Sep-2023 Adi : Remove ON/OFF logic generate Customer Matrix, because it is used for Screening Delta
	# if isUsedCustomerAsMatrix==1:
	load_matrix_aml_customer()		

	#08-Dec-2022 Adi : Tambahin LoadWatchlist juga
	load_watchlist_data()		

class LoadMatrix(Resource):
	def get(self):
		t1 = time.time()

		load_matrix_all()

		t = time.time() - t1
		print("Load Matrix ALL Done in " + str(t) + " seconds.")

		return "Load Matrix Done", 200  # return data and 200 OK
################### End of 15-Nov-2022 Adi : Tambah End Point untuk Load Matrix ALL untuk dicall lewat Load Balancer


################### 3-Jan-2023 Adi : Penambahan Screening Batch by Customer Matrix
# 26-Dec-2022 Adi : Tuning Performance untuk screening batch
def get_matches_df_Batch(uniqID, sparse_matrix, A, B, offset, requestID):
	non_zeros = sparse_matrix.nonzero()

	sparserows = non_zeros[0]
	sparsecols = non_zeros[1]

	nr_matches = sparsecols.size

	result_code = np.empty([nr_matches], dtype="S36")
	left_side_index = np.empty([nr_matches], dtype=int)
	right_side_index = np.empty([nr_matches], dtype=int)
	similairity = np.zeros(nr_matches)

	for index in range(0, nr_matches):
		result_code[index] = uniqID
		left_side_index[index] = A[int(sparserows[index])]
		right_side_index[index] = B[int(sparsecols[index]) + offset]
		similairity[index] = sparse_matrix.data[index]

	#3-Jan-2023 Adi : Left Side Index dan Right Side Index dibalik karena Customer yg jadi matrix
	df_result = pd.DataFrame({
				"RESULT_CODE": result_code,
				"LEFT_SIDE_INDEX": right_side_index,
				"RIGHT_SIDE_INDEX": left_side_index,
				"SIMILARITY": similairity})

	del non_zeros, sparserows, sparsecols, nr_matches, result_code, left_side_index, right_side_index, similairity

	return df_result

class Compare_Batch():
	def DoCompare(tf_idf_matrix, matrix, listID, fieldToCompare, uniqID, names, offset, requestID):
		t1 = time.time()
		singleton_param = SingletonParam()

		matches = awesome_cossim_top(tf_idf_matrix, matrix, singleton_param.maximumRowMatch, singleton_param.minimumSimilarity, True, singleton_param.maximumCore)

		t = time.time()-t1
		print("COMPARE: " + fieldToCompare + " " + str(t))
		t1 = time.time()

		matches_df = get_matches_df_Batch(uniqID, matches, names, listID, offset, requestID)

		t = time.time()-t1
		print("MATCHING: " + fieldToCompare + " " + str(t))

		del singleton_param, matches

		return matches_df

class Matching_Batch():
	def DoMatching(requestid, fieldToCompare, names, uniqID):
		t1 = time.time()
		singleton_param = SingletonParam()

		result_df = pd.DataFrame()

		if not names.empty:
			org_names = names[fieldToCompare]

			if fieldToCompare.upper() == "NAME":
				matrix = customer_tf_idf_matrix_name
				vectorizer = customer_vectorizer_name
				listID = customer_WatchlistListID_name
			elif fieldToCompare.upper() == "NATIONALITY":
				matrix = customer_tf_idf_matrix_nationality
				vectorizer = customer_vectorizer_nationality
				listID = customer_WatchlistListID_nationality
			elif fieldToCompare.upper() == "IDENTITY_NUMBER":
				matrix = customer_tf_idf_matrix_identity_number
				vectorizer = customer_vectorizer_identity_number
				listID = customer_WatchlistListID_identity_number

			tf_idf_matrix = vectorizer.transform(org_names)

			t = time.time()-t1
			print("CREATE MATRIX: " + fieldToCompare + " " + str(t))
			t1 = time.time()

			for i in range(len(matrix)):
				matched_df = Compare_Batch.DoCompare(tf_idf_matrix, matrix[i], listID[i], fieldToCompare, uniqID, names["PK_AML_WATCHLIST_SCREENING_ID"], i * singleton_param.pageSize, requestid)
				result_df = pd.concat([result_df, matched_df])

		del singleton_param, matrix, vectorizer, listID, matched_df

		return result_df

class ScreeningBatchByCustomerMatrix(Resource):
	def get(self):
		result = ""

		singleton_param = SingletonParam()

		if InProgressRefreshMatrix_AML == 0:				
			t1 = time.time()

			parser = reqparse.RequestParser()  # initialize
			parser.add_argument('requestid', type=str)
			args = parser.parse_args()  # parse arguments to dictionary

			requestid = args['requestid']

			# open connection
			with pyodbc.connect(singleton_param.connString) as _conn:
				# Screening Name
				responseName = pd.DataFrame()
				responseNationality = pd.DataFrame()
				responseIdentity = pd.DataFrame()

				names = pd.read_sql_query("SELECT PK_AML_WATCHLIST_SCREENING_ID, ALIAS_NAME AS NAME FROM AML_WATCHLIST_SCREENING WITH (NOLOCK) WHERE ISNULL(ALIAS_NAME, '') <> '' ORDER BY PK_AML_WATCHLIST_SCREENING_ID", _conn)
				uniqIDName = str(uuid.uuid4())
				if not names.empty:
					responseName = Matching_Batch.DoMatching(requestid, "NAME", names, uniqIDName)

				# Screening Nationality
				nationalities = pd.read_sql_query("SELECT PK_AML_WATCHLIST_SCREENING_ID, NATIONALITY FROM AML_WATCHLIST_SCREENING WITH (NOLOCK) WHERE ISNULL(NATIONALITY, '') <> '' ORDER BY PK_AML_WATCHLIST_SCREENING_ID", _conn)
				uniqIDNationality = str(uuid.uuid4())
				if not names.empty:
					responseNationality = Matching_Batch.DoMatching(requestid, "NATIONALITY", nationalities, uniqIDNationality)

				# Screening Identity Number
				idnumbers = pd.read_sql_query("SELECT PK_AML_WATCHLIST_SCREENING_ID, IDENTITYNUMBER AS IDENTITY_NUMBER FROM AML_WATCHLIST_SCREENING WITH (NOLOCK) WHERE ISNULL(IDENTITYNUMBER, '') <> '' ORDER BY PK_AML_WATCHLIST_SCREENING_ID", _conn)
				uniqIDIdentity = str(uuid.uuid4())
				if not names.empty:
					responseIdentity = Matching_Batch.DoMatching(requestid, "IDENTITY_NUMBER", idnumbers, uniqIDIdentity)

				# Write to DB
				engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" % urllib.parse.quote_plus(singleton_param.connString))

				if not responseName.empty: 
					print("WRITE Screening Name Result to DB Start...")
					responseName.to_sql('AML_SCREENING_RESULT_API', con=engine, if_exists='append', index=False)
					print("WRITE Screening Name Result to DB Finished.")

				if not responseNationality.empty: 
					print("WRITE Screening Nationality Result to DB Start...")				
					responseNationality.to_sql('AML_SCREENING_RESULT_API', con=engine, if_exists='append', index=False)
					print("WRITE Screening Nationality Result to DB Finished.")

				if not responseIdentity.empty: 
					print("WRITE Screening Identity Number Result to DB Start...")				
					responseIdentity.to_sql('AML_SCREENING_RESULT_API', con=engine, if_exists='append', index=False)
					print("WRITE Screening Identity Number Result to DB Finished.")

			result = uniqIDName + "," + uniqIDNationality + "," + uniqIDIdentity

			del args, requestid
			del responseName, responseNationality, responseIdentity
			del names, uniqIDName, nationalities, uniqIDNationality, idnumbers, uniqIDIdentity

		else :	#Jika in progress reload matrix
			result = "In Progress Refresh Matrix"

		del singleton_param

		return result, 200	
# End of 26-Dec-2022 Adi : Tuning Performance untuk screening batch
################### End of 3-Jan-2023 Adi : Penambahan Screening Batch by Customer Matrix


################### 3-Jan-2023 Adi : Penambahan Screening Batch Delta by Customer Matrix
# Screening hanya Watchlist2 yang 
class ScreeningBatchDeltaByCustomerMatrix(Resource):
	def get(self):
		t1 = time.time()

		singleton_param = SingletonParam()

		#String to Return
		result = ""

		if InProgressRefreshMatrix_AML == 0:				
			t1 = time.time()

			parser = reqparse.RequestParser()  # initialize
			parser.add_argument('requestid', type=str)
			parser.add_argument('processdate', type=str)
			args = parser.parse_args()  # parse arguments to dictionary

			requestid = args['requestid']
			processDate = args['processdate']

			# open connection
			# open connection
			with pyodbc.connect(singleton_param.connString) as _conn:
				# Screening Name
				responseName = pd.DataFrame()
				responseNationality = pd.DataFrame()
				responseIdentity = pd.DataFrame()

				names = pd.read_sql_query("SELECT aws.PK_AML_WATCHLIST_SCREENING_ID, aws.ALIAS_NAME AS [NAME] FROM AML_WATCHLIST_SCREENING aws WITH (NOLOCK) JOIN AML_WATCHLIST AS aw WITH (NOLOCK) ON aws.FK_AML_WATCHLIST_ID = aw.PK_AML_WATCHLIST_ID WHERE ISNULL(aws.ALIAS_NAME, '') <> '' AND (DATEDIFF(DAY, aw.CreatedDate, '" + processDate + "') = 0 OR DATEDIFF(DAY, aw.LastUpdateDate, '" + processDate + "') = 0) ORDER BY PK_AML_WATCHLIST_SCREENING_ID", _conn)
				uniqIDName = str(uuid.uuid4())
				if not names.empty:
					responseName = Matching_Batch.DoMatching(requestid, "NAME", names, uniqIDName)

				# Screening Nationality
				nationalities = pd.read_sql_query("SELECT aws.PK_AML_WATCHLIST_SCREENING_ID, aws.NATIONALITY FROM AML_WATCHLIST_SCREENING aws WITH (NOLOCK) JOIN AML_WATCHLIST AS aw WITH (NOLOCK) ON aws.FK_AML_WATCHLIST_ID = aw.PK_AML_WATCHLIST_ID WHERE ISNULL(aws.NATIONALITY, '') <> '' AND (DATEDIFF(DAY, aw.CreatedDate, '" + processDate + "') = 0 OR DATEDIFF(DAY, aw.LastUpdateDate, '" + processDate + "') = 0) ORDER BY PK_AML_WATCHLIST_SCREENING_ID", _conn)
				uniqIDNationality = str(uuid.uuid4())
				if not names.empty:
					responseNationality = Matching_Batch.DoMatching(requestid, "NATIONALITY", nationalities, uniqIDNationality)

				# Screening Identity Number
				idnumbers = pd.read_sql_query("SELECT aws.PK_AML_WATCHLIST_SCREENING_ID, aws.IDENTITYNUMBER AS IDENTITY_NUMBER FROM AML_WATCHLIST_SCREENING aws WITH (NOLOCK) JOIN AML_WATCHLIST AS aw WITH (NOLOCK) ON aws.FK_AML_WATCHLIST_ID = aw.PK_AML_WATCHLIST_ID WHERE ISNULL(aws.IDENTITYNUMBER, '') <> '' AND (DATEDIFF(DAY, aw.CreatedDate, '" + processDate + "') = 0 OR DATEDIFF(DAY, aw.LastUpdateDate, '" + processDate + "') = 0) ORDER BY PK_AML_WATCHLIST_SCREENING_ID", _conn)
				uniqIDIdentity = str(uuid.uuid4())
				if not names.empty:
					responseIdentity = Matching_Batch.DoMatching(requestid, "IDENTITY_NUMBER", idnumbers, uniqIDIdentity)

				# Write to DB
				engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" % urllib.parse.quote_plus(singleton_param.connString))

				if not responseName.empty: 
					print("WRITE Screening Name Result to DB Start...")
					responseName.to_sql('AML_SCREENING_RESULT_API', con=engine, if_exists='append', index=False)
					print("WRITE Screening Name Result to DB Finished.")

				if not responseNationality.empty: 
					print("WRITE Screening Nationality Result to DB Start...")				
					responseNationality.to_sql('AML_SCREENING_RESULT_API', con=engine, if_exists='append', index=False)
					print("WRITE Screening Nationality Result to DB Finished.")

				if not responseIdentity.empty: 
					print("WRITE Screening Identity Number Result to DB Start...")				
					responseIdentity.to_sql('AML_SCREENING_RESULT_API', con=engine, if_exists='append', index=False)
					print("WRITE Screening Identity Number Result to DB Finished.")

			result = uniqIDName + "," + uniqIDNationality + "," + uniqIDIdentity

			del args, requestid
			del responseName, responseNationality, responseIdentity
			del names, uniqIDName, nationalities, uniqIDNationality, idnumbers, uniqIDIdentity

		else :	#Jika in progress reload matrix
			result = "In Progress Refresh Matrix"

		t = time.time() - t1
		print("Screening Watchlist Delta Done in " + str(t) + " seconds.")

		return result, 200	
################### End of 6-Feb-2023 Adi : Penambahan Screening Batch Delta by Customer Matrix


############################################ SIPENDAR #######################################################
################### 10 Agustus 2021 : Tambahan untuk Screening SiPendar
# 15-Nov-2022 Adi : Define function for loading SIPENDAR CUSTOMER Matrix so it can called from Load Balancer
def load_matrix_sipendar():
	print("Load SIPENDAR customer matrix...")

	global sipendar_tf_idf_matrix_nama
	global sipendar_vectorizer_nama
	global sipendar_WatchlistListID_nama
	
	global sipendar_tf_idf_matrix_birth_place
	global sipendar_vectorizer_birth_place
	global sipendar_WatchlistListID_birth_place

	global sipendar_tf_idf_matrix_nationality
	global sipendar_vectorizer_nationality
	global sipendar_WatchlistListID_nationality

	global sipendar_tf_idf_matrix_identity_number
	global sipendar_vectorizer_identity_number
	global sipendar_WatchlistListID_identity_number

	sipendar_tf_idf_matrix_nama = []
	sipendar_WatchlistListID_nama = []

	sipendar_tf_idf_matrix_birth_place = []
	sipendar_WatchlistListID_birth_place = []

	sipendar_tf_idf_matrix_nationality = []
	sipendar_WatchlistListID_nationality = []

	sipendar_tf_idf_matrix_identity_number = []
	sipendar_WatchlistListID_identity_number = []	

	singleton_param = SingletonParam()
	with pyodbc.connect(singleton_param.connString) as _conn:
		with _conn.cursor() as _cursor:
			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'Customer_Nama_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_tempMatrix = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_tempMatrix):
					sipendar_tempMatrix = sipendar_tempMatrix.tocsr()
				sipendar_tf_idf_matrix_nama.append(sipendar_tempMatrix)
				sipendar_WatchlistListID_nama.append(pickle.loads(row[1]))
				if(len(sipendar_tf_idf_matrix_nama) == 1):
					sipendar_vectorizer_nama = pickle.loads(row[2])

				del sipendar_tempMatrix

			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'Customer_Birth_Place_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_tempMatrix = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_tempMatrix):
					sipendar_tempMatrix = sipendar_tempMatrix.tocsr()
				sipendar_tf_idf_matrix_birth_place.append(sipendar_tempMatrix)
				sipendar_WatchlistListID_birth_place.append(pickle.loads(row[1]))
				if(len(sipendar_tf_idf_matrix_birth_place) == 1):
					sipendar_vectorizer_birth_place = pickle.loads(row[2])

				del sipendar_tempMatrix

			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'Customer_Nationality_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_tempMatrix = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_tempMatrix):
					sipendar_tempMatrix = sipendar_tempMatrix.tocsr()
				sipendar_tf_idf_matrix_nationality.append(sipendar_tempMatrix)
				sipendar_WatchlistListID_nationality.append(pickle.loads(row[1]))
				if(len(sipendar_tf_idf_matrix_nationality) == 1):
					sipendar_vectorizer_nationality = pickle.loads(row[2])

				del sipendar_tempMatrix

			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'Customer_Identity_Number_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_tempMatrix = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_tempMatrix):
					sipendar_tempMatrix = sipendar_tempMatrix.tocsr()
				sipendar_tf_idf_matrix_identity_number.append(sipendar_tempMatrix)
				sipendar_WatchlistListID_identity_number.append(pickle.loads(row[1]))
				if(len(sipendar_tf_idf_matrix_identity_number) == 1):
					sipendar_vectorizer_identity_number = pickle.loads(row[2])

				del sipendar_tempMatrix

	del singleton_param

# Run load matrix sipendar
#load_matrix_sipendar()	--14-Sep-2023 : Has been load on main function

class MatrixSipendar(Resource):
	def get(self):
		global sipendar_tf_idf_matrix_nama
		global sipendar_vectorizer_nama
		global sipendar_WatchlistListID_nama
		
		global sipendar_tf_idf_matrix_birth_place
		global sipendar_vectorizer_birth_place
		global sipendar_WatchlistListID_birth_place

		global sipendar_tf_idf_matrix_nationality
		global sipendar_vectorizer_nationality
		global sipendar_WatchlistListID_nationality

		global sipendar_tf_idf_matrix_identity_number
		global sipendar_vectorizer_identity_number
		global sipendar_WatchlistListID_identity_number

		parser = reqparse.RequestParser()  # initialize
		parser.add_argument('fieldid', type=str)
		parser.add_argument('fieldidalias', type=str)
		parser.add_argument('fieldtocompare', type=str)
		parser.add_argument('fieldtocomparealias', type=str)
		parser.add_argument('table', type=str)
		args = parser.parse_args()  # parse arguments to dictionary

		fieldId = args['fieldid'].upper()
		fieldIdAlias = args['fieldidalias'].upper()
		fieldToCompare = args['fieldtocompare'].upper()
		fieldToCompareAlias = args['fieldtocomparealias'].upper()
		table = args['table'].upper()

		t1 = time.time()
		singleton_param = SingletonParam()
		with pyodbc.connect(singleton_param.connString) as _conn:

			names = pd.read_sql_query("SELECT " + fieldId + " AS " + fieldIdAlias + ", " + fieldToCompare + " AS " + fieldToCompareAlias + " FROM " + table + " WHERE ISNULL(" + fieldToCompare + ", '') <> '' ORDER BY " + fieldIdAlias, _conn)

			t = time.time()-t1
			print("SELECT SIPENDAR MATRIX DATA: " + fieldToCompareAlias + " " + str(t))
			t1 = time.time()

			if not names.empty:
				org_names = names[fieldToCompareAlias]

				vectorizer = TfidfVectorizer(min_df=1, analyzer="char_wb", ngram_range=(3,3))
				matrix = vectorizer.fit_transform(org_names)
				listID = names[fieldIdAlias]

				t = time.time()-t1
				print("GENERATE SIPENDAR MATRIX: " + fieldToCompareAlias + " " + str(t))
				t1 = time.time()

				with _conn.cursor() as _cursor:
					_cursor.execute("DELETE SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE ?", "CUSTOMER_" + fieldToCompareAlias + "_%")

				t = time.time()-t1
				print("DELETE SIPENDAR PREV DATA: " + fieldToCompareAlias + " " + str(t))
				t1 = time.time()

				dataSize = len(org_names)
				counter = 0
				counterIndex = 0

				vectorizerDump = pickle.dumps(vectorizer)

				if fieldToCompareAlias.upper() == "NAMA":
					sipendar_tf_idf_matrix_nama = []
					sipendar_WatchlistListID_nama = []
					sipendar_vectorizer_nama = vectorizer
				elif fieldToCompareAlias.upper() == "BIRTH_PLACE":
					sipendar_tf_idf_matrix_birth_place = []
					sipendar_WatchlistListID_birth_place = []
					sipendar_vectorizer_birth_place = vectorizer
				elif fieldToCompareAlias.upper() == "NATIONALITY":
					sipendar_tf_idf_matrix_nationality = []
					sipendar_WatchlistListID_nationality = []
					sipendar_vectorizer_nationality = vectorizer
				elif fieldToCompareAlias.upper() == "IDENTITY_NUMBER":
					sipendar_tf_idf_matrix_identity_number = []
					sipendar_WatchlistListID_identity_number = []
					sipendar_vectorizer_identity_number = vectorizer

				while counter < dataSize:
					t = time.time()-t1
					print("Processing data : " + str(counter) + " / " + str(dataSize) + " " + str(t))
					t1 = time.time()

					matrixSliced = matrix[counter: counter + singleton_param.pageSize].transpose()
					listIDSliced = listID[counter: counter + singleton_param.pageSize]

					t = time.time()-t1
					print("Slice Data : " + str(counter) + " / " + str(dataSize) + " " + str(t))
					t1 = time.time()

					matrixDump = pickle.dumps(matrixSliced)
					listIDDump = pickle.dumps(listIDSliced)

					with _conn.cursor() as _cursor:
						_cursor.execute("INSERT INTO SIPENDAR_CUSTOMER_MATRIX (MATRIX_NAME, MATRIX, VECTORIZER, LIST_ID) VALUES(?, ?, ?, ?)", ('CUSTOMER_' + fieldToCompareAlias + "_" + str(counterIndex + 1).zfill(2), pyodbc.Binary(matrixDump), pyodbc.Binary(vectorizerDump), pyodbc.Binary(listIDDump)))

					t = time.time()-t1
					print("Insert DB : " + str(counter) + " / " + str(dataSize) + " " + str(t))
					t1 = time.time()

					if fieldToCompareAlias.upper() == "NAMA":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_tf_idf_matrix_nama.append(matrixSliced)
						sipendar_WatchlistListID_nama.append(listIDSliced)
					elif fieldToCompareAlias.upper() == "BIRTH_PLACE":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_tf_idf_matrix_birth_place.append(matrixSliced)
						sipendar_WatchlistListID_birth_place.append(listIDSliced)
					elif fieldToCompareAlias.upper() == "NATIONALITY":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_tf_idf_matrix_nationality.append(matrixSliced)
						sipendar_WatchlistListID_nationality.append(listIDSliced)
					elif fieldToCompareAlias.upper() == "IDENTITY_NUMBER":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_tf_idf_matrix_identity_number.append(matrixSliced)
						sipendar_WatchlistListID_identity_number.append(listIDSliced)

					counter = counter + singleton_param.pageSize
					counterIndex = counterIndex + 1
		
					del matrixDump, listIDDump, matrixSliced, listIDSliced

				del org_names, vectorizer, matrix, listID, dataSize, counter, counterIndex, vectorizerDump

			del names
			
		del singleton_param

		t = time.time()-t1
		print("Done SIPENDAR Creating Matrix : " + fieldToCompareAlias + " " + str(t))
		t1 = time.time()

		return "SUCCESS", 200  # return data and 200 OK

class CompareSipendar():
	def DoCompare(tf_idf_matrix, matrix, listID, fieldToCompare, uniqID, names, offset):
		t1 = time.time()

		singleton_param = SingletonParam()

		matches = awesome_cossim_top(tf_idf_matrix, matrix, singleton_param.maximumRowMatch, singleton_param.minimumSimilarity, True, singleton_param.maximumCore)

		t = time.time()-t1
		print("COMPARE SIPENDAR : " + fieldToCompare + " " + str(t))
		t1 = time.time()

		matches_df = get_matches_df(uniqID, matches, names, listID, offset)

		t = time.time()-t1
		print("MATCHING SIPENDAR : " + fieldToCompare + " " + str(t))
		t1 = time.time()

		engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" % urllib.parse.quote_plus(singleton_param.connString), fast_executemany=True)
		matches_df.to_sql('SIPENDAR_SCREENING_RESULT_API', con=engine, if_exists='append', index=False)

		t = time.time()-t1
		print("INSERT SIPENDAR DB: " + fieldToCompare + " " + str(t))
		t1 = time.time()

		del singleton_param, matches, matches_df, engine

class ScreeningSipendar(Resource):
	def get(self):
		parser = reqparse.RequestParser()  # initialize
		parser.add_argument('requestid', type=str)
		parser.add_argument('fieldtocompare', type=str)
		args = parser.parse_args()  # parse arguments to dictionary

		uniqID = str(uuid.uuid4())
		requestid = args['requestid']
		fieldToCompare = args['fieldtocompare'].upper()

		t1 = time.time()
		singleton_param = SingletonParam()

		with pyodbc.connect(singleton_param.connString) as _conn:
			names = pd.read_sql_query("SELECT PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID, " + fieldToCompare + " FROM SIPENDAR_SCREENING_REQUEST_DETAIL WHERE FK_SIPENDAR_SCREENING_REQUEST_ID = " + requestid + " AND ISNULL(" + fieldToCompare + ", '') <> '' ORDER BY PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID", _conn)

			t = time.time()-t1
			print("SELECT SIPENDAR SCREENING: " + fieldToCompare + " " + str(t))
			t1 = time.time()

			if not names.empty:
				org_names = names[fieldToCompare]

				if fieldToCompare.upper() == "NAMA":
					matrix = sipendar_tf_idf_matrix_nama
					vectorizer = sipendar_vectorizer_nama
					listID = sipendar_WatchlistListID_nama
				elif fieldToCompare.upper() == "BIRTH_PLACE":
					matrix = sipendar_tf_idf_matrix_birth_place
					vectorizer = sipendar_vectorizer_birth_place
					listID = sipendar_WatchlistListID_birth_place
				elif fieldToCompare.upper() == "NATIONALITY":
					matrix = sipendar_tf_idf_matrix_nationality
					vectorizer = sipendar_vectorizer_nationality
					listID = sipendar_WatchlistListID_nationality
				elif fieldToCompare.upper() == "IDENTITY_NUMBER":
					matrix = sipendar_tf_idf_matrix_identity_number
					vectorizer = sipendar_vectorizer_identity_number
					listID = sipendar_WatchlistListID_identity_number

				tf_idf_matrix = vectorizer.transform(org_names)

				t = time.time()-t1
				print("CREATE SIPENDAR MATRIX: " + fieldToCompare + " " + str(t))
				t1 = time.time()

				# with ThreadPoolExecutor(max_workers=len(matrix)) as executor:
				# 	for i in range(len(matrix)):
				# 		executor.submit(Compare.DoCompare, tf_idf_matrix, matrix[i], listID[i], fieldToCompare, uniqID, names["PK_AML_SCREENING_REQUEST_ID"], i * pageSize)

				# 	executor.shutdown()

				for i in range(len(matrix)):
					CompareSipendar.DoCompare(tf_idf_matrix, matrix[i], listID[i], fieldToCompare, uniqID, names["PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID"], i * singleton_param.pageSize)

			del matrix, vectorizer, listID, org_names, tf_idf_matrix

		t = time.time()-t1
		print("DONE SIPENDAR SCREENING: " + fieldToCompare + " " + str(t))

		del args, singleton_param, names

		return uniqID, 200  # return data and 200 OK
################### 10 Agustus 2021 : Tambahan untuk Screening SiPendar


################### 21 Oktober 2021 : Tambahan untuk Screening Delta (New Customer Only) SiPendar
# 15-Nov-2022 Adi : Define function for loading SIPENDAR Customer Delta Matrix so it can called from Load Balancer
def load_matrix_sipendar_delta():
	print("Load SIPENDAR customer delta matrix...")

	global sipendar_delta_tf_idf_matrix_nama
	global sipendar_delta_vectorizer_nama
	global sipendar_delta_WatchlistListID_nama
	
	global sipendar_delta_tf_idf_matrix_birth_place
	global sipendar_delta_vectorizer_birth_place
	global sipendar_delta_WatchlistListID_birth_place

	global sipendar_delta_tf_idf_matrix_nationality
	global sipendar_delta_vectorizer_nationality
	global sipendar_delta_WatchlistListID_nationality

	global sipendar_delta_tf_idf_matrix_identity_number
	global sipendar_delta_vectorizer_identity_number
	global sipendar_delta_WatchlistListID_identity_number

	sipendar_delta_tf_idf_matrix_nama = []
	sipendar_delta_WatchlistListID_nama = []

	sipendar_delta_tf_idf_matrix_birth_place = []
	sipendar_delta_WatchlistListID_birth_place = []

	sipendar_delta_tf_idf_matrix_nationality = []
	sipendar_delta_WatchlistListID_nationality = []

	sipendar_delta_tf_idf_matrix_identity_number = []
	sipendar_delta_WatchlistListID_identity_number = []	

	singleton_param = SingletonParam()
	with pyodbc.connect(singleton_param.connString) as _conn:
		with _conn.cursor() as _cursor:
			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'NewCustomer_Nama_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_delta_tempMatrix = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_delta_tempMatrix):
					sipendar_delta_tempMatrix = sipendar_delta_tempMatrix.tocsr()
				sipendar_delta_tf_idf_matrix_nama.append(sipendar_delta_tempMatrix)
				sipendar_delta_WatchlistListID_nama.append(pickle.loads(row[1]))
				if(len(sipendar_delta_tf_idf_matrix_nama) == 1):
					sipendar_delta_vectorizer_nama = pickle.loads(row[2])

			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'NewCustomer_Birth_Place_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_delta_tempMatrix = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_delta_tempMatrix):
					sipendar_delta_tempMatrix = sipendar_delta_tempMatrix.tocsr()
				sipendar_delta_tf_idf_matrix_birth_place.append(sipendar_delta_tempMatrix)
				sipendar_delta_WatchlistListID_birth_place.append(pickle.loads(row[1]))
				if(len(sipendar_delta_tf_idf_matrix_birth_place) == 1):
					sipendar_delta_vectorizer_birth_place = pickle.loads(row[2])

				del sipendar_delta_tempMatrix

			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'NewCustomer_Nationality_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_delta_tempMatrix = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_delta_tempMatrix):
					sipendar_delta_tempMatrix = sipendar_delta_tempMatrix.tocsr()
				sipendar_delta_tf_idf_matrix_nationality.append(sipendar_delta_tempMatrix)
				sipendar_delta_WatchlistListID_nationality.append(pickle.loads(row[1]))
				if(len(sipendar_delta_tf_idf_matrix_nationality) == 1):
					sipendar_delta_vectorizer_nationality = pickle.loads(row[2])

				del sipendar_delta_tempMatrix

			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'NewCustomer_Identity_Number_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_delta_tempMatrix = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_delta_tempMatrix):
					sipendar_delta_tempMatrix = sipendar_delta_tempMatrix.tocsr()
				sipendar_delta_tf_idf_matrix_identity_number.append(sipendar_delta_tempMatrix)
				sipendar_delta_WatchlistListID_identity_number.append(pickle.loads(row[1]))
				if(len(sipendar_delta_tf_idf_matrix_identity_number) == 1):
					sipendar_delta_vectorizer_identity_number = pickle.loads(row[2])

				del sipendar_delta_tempMatrix

	del singleton_param

# Run load sipendar customer delta matrix
#load_matrix_sipendar_delta()	--14-Sep-2023 : Has been load on main function

class MatrixSipendarDelta(Resource):
	def get(self):
		global sipendar_delta_tf_idf_matrix_nama
		global sipendar_delta_vectorizer_nama
		global sipendar_delta_WatchlistListID_nama
		
		global sipendar_delta_tf_idf_matrix_birth_place
		global sipendar_delta_vectorizer_birth_place
		global sipendar_delta_WatchlistListID_birth_place

		global sipendar_delta_tf_idf_matrix_nationality
		global sipendar_delta_vectorizer_nationality
		global sipendar_delta_WatchlistListID_nationality

		global sipendar_delta_tf_idf_matrix_identity_number
		global sipendar_delta_vectorizer_identity_number
		global sipendar_delta_WatchlistListID_identity_number

		parser = reqparse.RequestParser()  # initialize
		parser.add_argument('fieldid', type=str)
		parser.add_argument('fieldidalias', type=str)
		parser.add_argument('fieldtocompare', type=str)
		parser.add_argument('fieldtocomparealias', type=str)
		parser.add_argument('table', type=str)
		args = parser.parse_args()  # parse arguments to dictionary

		fieldId = args['fieldid'].upper()
		fieldIdAlias = args['fieldidalias'].upper()
		fieldToCompare = args['fieldtocompare'].upper()
		fieldToCompareAlias = args['fieldtocomparealias'].upper()
		table = args['table'].upper()

		t1 = time.time()
		singleton_param = SingletonParam()
		with pyodbc.connect(singleton_param.connString) as _conn:
			names = pd.read_sql_query("SELECT " + fieldId + " AS " + fieldIdAlias + ", " + fieldToCompare + " AS " + fieldToCompareAlias + " FROM " + table + " WHERE ISNULL(" + fieldToCompare + ", '') <> '' ORDER BY " + fieldIdAlias, _conn)

			t = time.time()-t1
			print("SELECT SIPENDAR MATRIX DATA (DELTA): " + fieldToCompareAlias + " " + str(t))
			t1 = time.time()

			if not names.empty:
				org_names = names[fieldToCompareAlias]

				vectorizer = TfidfVectorizer(min_df=1, analyzer="char_wb", ngram_range=(3,3))
				matrix = vectorizer.fit_transform(org_names)
				listID = names[fieldIdAlias]

				t = time.time()-t1
				print("GENERATE SIPENDAR MATRIX (DELTA): " + fieldToCompareAlias + " " + str(t))
				t1 = time.time()

				with _conn.cursor() as _cursor:
					_cursor.execute("DELETE SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE ?", "NewCustomer_" + fieldToCompareAlias + "_%")

				t = time.time()-t1
				print("DELETE SIPENDAR PREV DATA (DELTA): " + fieldToCompareAlias + " " + str(t))
				t1 = time.time()

				dataSize = len(org_names)
				counter = 0
				counterIndex = 0

				vectorizerDump = pickle.dumps(vectorizer)

				if fieldToCompareAlias.upper() == "NAMA":
					sipendar_delta_tf_idf_matrix_nama = []
					sipendar_delta_WatchlistListID_nama = []
					sipendar_delta_vectorizer_nama = vectorizer
				elif fieldToCompareAlias.upper() == "BIRTH_PLACE":
					sipendar_delta_tf_idf_matrix_birth_place = []
					sipendar_delta_WatchlistListID_birth_place = []
					sipendar_delta_vectorizer_birth_place = vectorizer
				elif fieldToCompareAlias.upper() == "NATIONALITY":
					sipendar_delta_tf_idf_matrix_nationality = []
					sipendar_delta_WatchlistListID_nationality = []
					sipendar_delta_vectorizer_nationality = vectorizer
				elif fieldToCompareAlias.upper() == "IDENTITY_NUMBER":
					sipendar_delta_tf_idf_matrix_identity_number = []
					sipendar_delta_WatchlistListID_identity_number = []
					sipendar_delta_vectorizer_identity_number = vectorizer

				while counter < dataSize:
					t = time.time()-t1
					print("Processing data  (DELTA): " + str(counter) + " / " + str(dataSize) + " " + str(t))
					t1 = time.time()

					matrixSliced = matrix[counter: counter + singleton_param.pageSize].transpose()
					listIDSliced = listID[counter: counter + singleton_param.pageSize]

					t = time.time()-t1
					print("Slice Data  (DELTA): " + str(counter) + " / " + str(dataSize) + " " + str(t))
					t1 = time.time()

					matrixDump = pickle.dumps(matrixSliced)
					listIDDump = pickle.dumps(listIDSliced)

					with _conn.cursor() as _cursor:
						_cursor.execute("INSERT INTO SIPENDAR_CUSTOMER_MATRIX (MATRIX_NAME, MATRIX, VECTORIZER, LIST_ID) VALUES(?, ?, ?, ?)", ('NewCustomer_' + fieldToCompareAlias + "_" + str(counterIndex + 1).zfill(2), pyodbc.Binary(matrixDump), pyodbc.Binary(vectorizerDump), pyodbc.Binary(listIDDump)))

					t = time.time()-t1
					print("Insert DB  (DELTA): " + str(counter) + " / " + str(dataSize) + " " + str(t))
					t1 = time.time()

					if fieldToCompareAlias.upper() == "NAMA":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_delta_tf_idf_matrix_nama.append(matrixSliced)
						sipendar_delta_WatchlistListID_nama.append(listIDSliced)
					elif fieldToCompareAlias.upper() == "BIRTH_PLACE":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_delta_tf_idf_matrix_birth_place.append(matrixSliced)
						sipendar_delta_WatchlistListID_birth_place.append(listIDSliced)
					elif fieldToCompareAlias.upper() == "NATIONALITY":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_delta_tf_idf_matrix_nationality.append(matrixSliced)
						sipendar_delta_WatchlistListID_nationality.append(listIDSliced)
					elif fieldToCompareAlias.upper() == "IDENTITY_NUMBER":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_delta_tf_idf_matrix_identity_number.append(matrixSliced)
						sipendar_delta_WatchlistListID_identity_number.append(listIDSliced)

					counter = counter + singleton_param.pageSize
					counterIndex = counterIndex + 1
			
					del matrixDump, listIDDump, matrixSliced, listIDSliced

				del org_names, vectorizer, matrix, listID, dataSize, counter, counterIndex, vectorizerDump

			del names
			
		del singleton_param


		t = time.time()-t1
		print("Done SIPENDAR Creating Matrix  (DELTA): " + fieldToCompareAlias + " " + str(t))
		t1 = time.time()

		return "SUCCESS", 200  # return data and 200 OK

class CompareSipendarDelta():
	def DoCompare(tf_idf_matrix, matrix, listID, fieldToCompare, uniqID, names, offset):
		t1 = time.time()

		singleton_param = SingletonParam()

		matches = awesome_cossim_top(tf_idf_matrix, matrix, singleton_param.maximumRowMatch, singleton_param.minimumSimilarity, True, singleton_param.maximumCore)

		t = time.time()-t1
		print("COMPARE SIPENDAR  (DELTA): " + fieldToCompare + " " + str(t))
		t1 = time.time()

		matches_df = get_matches_df(uniqID, matches, names, listID, offset)

		t = time.time()-t1
		print("MATCHING SIPENDAR  (DELTA): " + fieldToCompare + " " + str(t))
		t1 = time.time()

		engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" % urllib.parse.quote_plus(singleton_param.connString), fast_executemany=True)

		matches_df.to_sql('SIPENDAR_SCREENING_RESULT_API', con=engine, if_exists='append', index=False)

		t = time.time()-t1
		print("INSERT SIPENDAR DB (DELTA): " + fieldToCompare + " " + str(t))
		t1 = time.time()

		del singleton_param, matches, matches_df, engine

class ScreeningSipendarDelta(Resource):
	def get(self):
		parser = reqparse.RequestParser()  # initialize
		parser.add_argument('requestid', type=str)
		parser.add_argument('fieldtocompare', type=str)
		args = parser.parse_args()  # parse arguments to dictionary

		uniqID = str(uuid.uuid4())
		requestid = args['requestid']
		fieldToCompare = args['fieldtocompare'].upper()

		t1 = time.time()
		singleton_param = SingletonParam()
		with pyodbc.connect(singleton_param.connString) as _conn:

			names = pd.read_sql_query("SELECT PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID, " + fieldToCompare + " FROM SIPENDAR_SCREENING_REQUEST_DETAIL WHERE FK_SIPENDAR_SCREENING_REQUEST_ID = " + requestid + " AND ISNULL(" + fieldToCompare + ", '') <> '' ORDER BY PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID", _conn)

			t = time.time()-t1
			print("SELECT SIPENDAR SCREENING (DELTA): " + fieldToCompare + " " + str(t))
			t1 = time.time()

			if not names.empty:
				org_names = names[fieldToCompare]

				if fieldToCompare.upper() == "NAMA":
					matrix = sipendar_delta_tf_idf_matrix_nama
					vectorizer = sipendar_delta_vectorizer_nama
					listID = sipendar_delta_WatchlistListID_nama
				elif fieldToCompare.upper() == "BIRTH_PLACE":
					matrix = sipendar_delta_tf_idf_matrix_birth_place
					vectorizer = sipendar_delta_vectorizer_birth_place
					listID = sipendar_delta_WatchlistListID_birth_place
				elif fieldToCompare.upper() == "NATIONALITY":
					matrix = sipendar_delta_tf_idf_matrix_nationality
					vectorizer = sipendar_delta_vectorizer_nationality
					listID = sipendar_delta_WatchlistListID_nationality
				elif fieldToCompare.upper() == "IDENTITY_NUMBER":
					matrix = sipendar_delta_tf_idf_matrix_identity_number
					vectorizer = sipendar_delta_vectorizer_identity_number
					listID = sipendar_delta_WatchlistListID_identity_number

				tf_idf_matrix = vectorizer.transform(org_names)

				t = time.time()-t1
				print("CREATE SIPENDAR MATRIX (DELTA): " + fieldToCompare + " " + str(t))
				t1 = time.time()

				for i in range(len(matrix)):
					CompareSipendarDelta.DoCompare(tf_idf_matrix, matrix[i], listID[i], fieldToCompare, uniqID, names["PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID"], i * singleton_param.pageSize)

			del matrix, vectorizer, listID, org_names, tf_idf_matrix

		t = time.time()-t1
		print("DONE SIPENDAR SCREENING (DELTA): " + fieldToCompare + " " + str(t))

		del args, singleton_param, names

		return uniqID, 200  # return data and 200 OK
################### End of 21 Oktober 2021 : Tambahan untuk Screening Delta (New Customer Only) SiPendar

################### Start of 04 Nov 2021 : Tambahan untuk Screening Stakeholder Sipendar
# 15-Nov-2022 Adi : Define function for loading SIPENDAR Stakeholder Matrix so it can called from Load Balancer
def load_matrix_sipendar_stakeholder():
	print("Load SIPENDAR STAKEHOLDER matrix...")

	global sipendar_tf_idf_matrix_nama_nc
	global sipendar_vectorizer_nama_nc
	global sipendar_WatchlistListID_nama_nc
	
	global sipendar_tf_idf_matrix_birth_place_nc
	global sipendar_vectorizer_birth_place_nc
	global sipendar_WatchlistListID_birth_place_nc

	global sipendar_tf_idf_matrix_nationality_nc
	global sipendar_vectorizer_nationality_nc
	global sipendar_WatchlistListID_nationality_nc

	global sipendar_tf_idf_matrix_identity_number_nc
	global sipendar_vectorizer_identity_number_nc
	global sipendar_WatchlistListID_identity_number_nc

	sipendar_tf_idf_matrix_nama_nc = []
	sipendar_WatchlistListID_nama_nc = []

	sipendar_tf_idf_matrix_birth_place_nc = []
	sipendar_WatchlistListID_birth_place_nc = []

	sipendar_tf_idf_matrix_nationality_nc = []
	sipendar_WatchlistListID_nationality_nc = []

	sipendar_tf_idf_matrix_identity_number_nc = []
	sipendar_WatchlistListID_identity_number_nc = []	

	singleton_param = SingletonParam()
	with pyodbc.connect(singleton_param.connString) as _conn:
		with _conn.cursor() as _cursor:
			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'STAKEHOLDER_Nama_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_tempMatrix_nc = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_tempMatrix_nc):
					sipendar_tempMatrix_nc = sipendar_tempMatrix_nc.tocsr()
				sipendar_tf_idf_matrix_nama_nc.append(sipendar_tempMatrix_nc)
				sipendar_WatchlistListID_nama_nc.append(pickle.loads(row[1]))
				if(len(sipendar_tf_idf_matrix_nama_nc) == 1):
					sipendar_vectorizer_nama_nc = pickle.loads(row[2])

				del sipendar_tempMatrix_nc

			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'STAKEHOLDER_Birth_Place_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_tempMatrix_nc = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_tempMatrix_nc):
					sipendar_tempMatrix_nc = sipendar_tempMatrix_nc.tocsr()
				sipendar_tf_idf_matrix_birth_place_nc.append(sipendar_tempMatrix_nc)
				sipendar_WatchlistListID_birth_place_nc.append(pickle.loads(row[1]))
				if(len(sipendar_tf_idf_matrix_birth_place_nc) == 1):
					sipendar_vectorizer_birth_place_nc = pickle.loads(row[2])

				del sipendar_tempMatrix_nc

			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'STAKEHOLDER_Nationality_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_tempMatrix_nc = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_tempMatrix_nc):
					sipendar_tempMatrix_nc = sipendar_tempMatrix_nc.tocsr()
				sipendar_tf_idf_matrix_nationality_nc.append(sipendar_tempMatrix_nc)
				sipendar_WatchlistListID_nationality_nc.append(pickle.loads(row[1]))
				if(len(sipendar_tf_idf_matrix_nationality_nc) == 1):
					sipendar_vectorizer_nationality_nc = pickle.loads(row[2])

				del sipendar_tempMatrix_nc

			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'STAKEHOLDER_Identity_Number_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_tempMatrix_nc = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_tempMatrix_nc):
					sipendar_tempMatrix_nc = sipendar_tempMatrix_nc.tocsr()
				sipendar_tf_idf_matrix_identity_number_nc.append(sipendar_tempMatrix_nc)
				sipendar_WatchlistListID_identity_number_nc.append(pickle.loads(row[1]))
				if(len(sipendar_tf_idf_matrix_identity_number_nc) == 1):
					sipendar_vectorizer_identity_number_nc = pickle.loads(row[2])

				del sipendar_tempMatrix_nc

	del singleton_param

# Run load matrix sipendar stakeholder
#load_matrix_sipendar_stakeholder()	--14-Sep-2023 : Has been load on main function

class MatrixSipendarStakeholder(Resource):
	def get(self):
		global sipendar_tf_idf_matrix_nama_nc
		global sipendar_vectorizer_nama_nc
		global sipendar_WatchlistListID_nama_nc
		
		global sipendar_tf_idf_matrix_birth_place_nc
		global sipendar_vectorizer_birth_place_nc
		global sipendar_WatchlistListID_birth_place_nc

		global sipendar_tf_idf_matrix_nationality_nc
		global sipendar_vectorizer_nationality_nc
		global sipendar_WatchlistListID_nationality_nc

		global sipendar_tf_idf_matrix_identity_number_nc
		global sipendar_vectorizer_identity_number_nc
		global sipendar_WatchlistListID_identity_number_nc

		parser = reqparse.RequestParser()  # initialize
		parser.add_argument('fieldid', type=str)
		parser.add_argument('fieldidalias', type=str)
		parser.add_argument('fieldtocompare', type=str)
		parser.add_argument('fieldtocomparealias', type=str)
		parser.add_argument('table', type=str)
		args = parser.parse_args()  # parse arguments to dictionary

		fieldId = args['fieldid'].upper()
		fieldIdAlias = args['fieldidalias'].upper()
		fieldToCompare = args['fieldtocompare'].upper()
		fieldToCompareAlias = args['fieldtocomparealias'].upper()
		table = args['table'].upper()

		t1 = time.time()
		singleton_param = SingletonParam()
		with pyodbc.connect(singleton_param.connString) as _conn:

			names = pd.read_sql_query("SELECT " + fieldId + " AS " + fieldIdAlias + ", " + fieldToCompare + " AS " + fieldToCompareAlias + " FROM " + table + " WHERE ISNULL(" + fieldToCompare + ", '') <> '' ORDER BY " + fieldIdAlias, _conn)

			t = time.time()-t1
			print("SELECT SIPENDAR MATRIX DATA (Stakeholder): " + fieldToCompareAlias + " " + str(t))
			t1 = time.time()

			if not names.empty:
				org_names = names[fieldToCompareAlias]

				vectorizer = TfidfVectorizer(min_df=1, analyzer="char_wb", ngram_range=(3,3))
				matrix = vectorizer.fit_transform(org_names)
				listID = names[fieldIdAlias]

				t = time.time()-t1
				print("GENERATE SIPENDAR MATRIX (Stakeholder): " + fieldToCompareAlias + " " + str(t))
				t1 = time.time()

				with _conn.cursor() as _cursor:
					_cursor.execute("DELETE SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE ?", "STAKEHOLDER_" + fieldToCompareAlias + "_%")

				t = time.time()-t1
				print("DELETE SIPENDAR PREV DATA (Stakeholder): " + fieldToCompareAlias + " " + str(t))
				t1 = time.time()

				dataSize = len(org_names)
				counter = 0
				counterIndex = 0

				vectorizerDump = pickle.dumps(vectorizer)

				if fieldToCompareAlias.upper() == "NAMA":
					sipendar_tf_idf_matrix_nama_nc = []
					sipendar_WatchlistListID_nama_nc = []
					sipendar_vectorizer_nama_nc = vectorizer
				elif fieldToCompareAlias.upper() == "BIRTH_PLACE":
					sipendar_tf_idf_matrix_birth_place_nc = []
					sipendar_WatchlistListID_birth_place_nc = []
					sipendar_vectorizer_birth_place_nc = vectorizer
				elif fieldToCompareAlias.upper() == "NATIONALITY":
					sipendar_tf_idf_matrix_nationality_nc = []
					sipendar_WatchlistListID_nationality_nc = []
					sipendar_vectorizer_nationality_nc = vectorizer
				elif fieldToCompareAlias.upper() == "IDENTITY_NUMBER":
					sipendar_tf_idf_matrix_identity_number_nc = []
					sipendar_WatchlistListID_identity_number_nc = []
					sipendar_vectorizer_identity_number_nc = vectorizer

				while counter < dataSize:
					t = time.time()-t1
					print("Processing data (Stakeholder): " + str(counter) + " / " + str(dataSize) + " " + str(t))
					t1 = time.time()

					matrixSliced = matrix[counter: counter + singleton_param.pageSize].transpose()
					listIDSliced = listID[counter: counter + singleton_param.pageSize]

					t = time.time()-t1
					print("Slice Data (Stakeholder): " + str(counter) + " / " + str(dataSize) + " " + str(t))
					t1 = time.time()

					matrixDump = pickle.dumps(matrixSliced)
					listIDDump = pickle.dumps(listIDSliced)

					with _conn.cursor() as _cursor:
						_cursor.execute("INSERT INTO SIPENDAR_CUSTOMER_MATRIX (MATRIX_NAME, MATRIX, VECTORIZER, LIST_ID) VALUES(?, ?, ?, ?)", ('STAKEHOLDER_' + fieldToCompareAlias + "_" + str(counterIndex + 1).zfill(2), pyodbc.Binary(matrixDump), pyodbc.Binary(vectorizerDump), pyodbc.Binary(listIDDump)))

					t = time.time()-t1
					print("Insert DB (Stakeholder): " + str(counter) + " / " + str(dataSize) + " " + str(t))
					t1 = time.time()

					if fieldToCompareAlias.upper() == "NAMA":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_tf_idf_matrix_nama_nc.append(matrixSliced)
						sipendar_WatchlistListID_nama_nc.append(listIDSliced)
					elif fieldToCompareAlias.upper() == "BIRTH_PLACE":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_tf_idf_matrix_birth_place_nc.append(matrixSliced)
						sipendar_WatchlistListID_birth_place_nc.append(listIDSliced)
					elif fieldToCompareAlias.upper() == "NATIONALITY":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_tf_idf_matrix_nationality_nc.append(matrixSliced)
						sipendar_WatchlistListID_nationality_nc.append(listIDSliced)
					elif fieldToCompareAlias.upper() == "IDENTITY_NUMBER":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_tf_idf_matrix_identity_number_nc.append(matrixSliced)
						sipendar_WatchlistListID_identity_number_nc.append(listIDSliced)

					counter = counter + singleton_param.pageSize
					counterIndex = counterIndex + 1
		
					del matrixDump, listIDDump, matrixSliced, listIDSliced

				del org_names, vectorizer, matrix, listID, dataSize, counter, counterIndex, vectorizerDump

			del names
			
		del singleton_param

		t = time.time()-t1
		print("Done SIPENDAR Creating Matrix : " + fieldToCompareAlias + " " + str(t))
		t1 = time.time()

		return "SUCCESS", 200  # return data and 200 OK
        
class CompareSipendarStakeholder():
	def DoCompare(tf_idf_matrix, matrix, listID, fieldToCompare, uniqID, names, offset):
		t1 = time.time()

		singleton_param = SingletonParam()

		matches = awesome_cossim_top(tf_idf_matrix, matrix, singleton_param.maximumRowMatch, singleton_param.minimumSimilarity, True, singleton_param.maximumCore)

		t = time.time()-t1
		print("COMPARE SIPENDAR Stakeholder : " + fieldToCompare + " " + str(t))
		t1 = time.time()

		matches_df = get_matches_df(uniqID, matches, names, listID, offset)

		t = time.time()-t1
		print("MATCHING SIPENDAR Stakeholder : " + fieldToCompare + " " + str(t))
		t1 = time.time()

		engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" % urllib.parse.quote_plus(singleton_param.connString), fast_executemany=True)

		matches_df.to_sql('SIPENDAR_SCREENING_RESULT_API', con=engine, if_exists='append', index=False)

		t = time.time()-t1
		print("INSERT SIPENDAR DB Stakeholder : " + fieldToCompare + " " + str(t))
		t1 = time.time()

		del singleton_param, matches, matches_df, engine


class ScreeningSipendarStakeholder(Resource):
	def get(self):
		parser = reqparse.RequestParser()  # initialize
		parser.add_argument('requestid', type=str)
		parser.add_argument('fieldtocompare', type=str)
		args = parser.parse_args()  # parse arguments to dictionary

		uniqID = str(uuid.uuid4())
		requestid = args['requestid']
		fieldToCompare = args['fieldtocompare'].upper()

		t1 = time.time()
		singleton_param = SingletonParam()
		with pyodbc.connect(singleton_param.connString) as _conn:

			names = pd.read_sql_query("SELECT PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID, " + fieldToCompare + " FROM SIPENDAR_SCREENING_REQUEST_DETAIL WHERE FK_SIPENDAR_SCREENING_REQUEST_ID = " + requestid + " AND ISNULL(" + fieldToCompare + ", '') <> '' ORDER BY PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID", _conn)

			t = time.time()-t1
			print("SELECT SIPENDAR SCREENING Stakeholder : " + fieldToCompare + " " + str(t))
			t1 = time.time()

			if not names.empty:
				org_names = names[fieldToCompare]

				if fieldToCompare.upper() == "NAMA":
					matrix = sipendar_tf_idf_matrix_nama_nc
					vectorizer = sipendar_vectorizer_nama_nc
					listID = sipendar_WatchlistListID_nama_nc
				elif fieldToCompare.upper() == "BIRTH_PLACE":
					matrix = sipendar_tf_idf_matrix_birth_place_nc
					vectorizer = sipendar_vectorizer_birth_place_nc
					listID = sipendar_WatchlistListID_birth_place_nc
				elif fieldToCompare.upper() == "NATIONALITY":
					matrix = sipendar_tf_idf_matrix_nationality_nc
					vectorizer = sipendar_vectorizer_nationality_nc
					listID = sipendar_WatchlistListID_nationality_nc
				elif fieldToCompare.upper() == "IDENTITY_NUMBER":
					matrix = sipendar_tf_idf_matrix_identity_number_nc
					vectorizer = sipendar_vectorizer_identity_number_nc
					listID = sipendar_WatchlistListID_identity_number_nc

				tf_idf_matrix = vectorizer.transform(org_names)

				t = time.time()-t1
				print("CREATE SIPENDAR MATRIX Stakeholder : " + fieldToCompare + " " + str(t))
				t1 = time.time()

				for i in range(len(matrix)):
					CompareSipendarStakeholder.DoCompare(tf_idf_matrix, matrix[i], listID[i], fieldToCompare, uniqID, names["PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID"], i * singleton_param.pageSize)

			del matrix, vectorizer, listID, org_names, tf_idf_matrix

		t = time.time()-t1
		print("DONE SIPENDAR SCREENING (STAKEHOLDER): " + fieldToCompare + " " + str(t))

		del args, singleton_param, names

		return uniqID, 200  # return data and 200 OK        
################### End of 04 Nov 2021 : Tambahan untuk Screening Stakeholder Sipendar


################### Start of 8 Nov 2021 : Tambahan untuk Customer Prospect
# 15-Nov-2022 Adi : Define function for loading SIPENDAR CUSTOMER PROSPECTMatrix so it can called from Load Balancer
def load_matrix_sipendar_prospect():
	print("Load SIPENDAR CUSTOMER PROSPECT matrix...")

	global sipendar_tf_idf_matrix_nama_cp
	global sipendar_vectorizer_nama_cp
	global sipendar_WatchlistListID_nama_cp
	
	global sipendar_tf_idf_matrix_birth_place_cp
	global sipendar_vectorizer_birth_place_cp
	global sipendar_WatchlistListID_birth_place_cp

	global sipendar_tf_idf_matrix_nationality_cp
	global sipendar_vectorizer_nationality_cp
	global sipendar_WatchlistListID_nationality_cp

	global sipendar_tf_idf_matrix_identity_number_cp
	global sipendar_vectorizer_identity_number_cp
	global sipendar_WatchlistListID_identity_number_cp

	sipendar_tf_idf_matrix_nama_cp = []
	sipendar_WatchlistListID_nama_cp = []

	sipendar_tf_idf_matrix_birth_place_cp = []
	sipendar_WatchlistListID_birth_place_cp = []

	sipendar_tf_idf_matrix_nationality_cp = []
	sipendar_WatchlistListID_nationality_cp = []

	sipendar_tf_idf_matrix_identity_number_cp = []
	sipendar_WatchlistListID_identity_number_cp = []	

	singleton_param = SingletonParam()

	with pyodbc.connect(singleton_param.connString) as _conn:
		with _conn.cursor() as _cursor:
			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'Watchlist_Nama_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_tempMatrix_cp = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_tempMatrix_cp):
					sipendar_tempMatrix_cp = sipendar_tempMatrix_cp.tocsr()
				sipendar_tf_idf_matrix_nama_cp.append(sipendar_tempMatrix_cp)
				sipendar_WatchlistListID_nama_cp.append(pickle.loads(row[1]))
				if(len(sipendar_tf_idf_matrix_nama_cp) == 1):
					sipendar_vectorizer_nama_cp = pickle.loads(row[2])

				del sipendar_tempMatrix_cp

			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'Watchlist_Birth_Place_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_tempMatrix_cp = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_tempMatrix_cp):
					sipendar_tempMatrix_cp = sipendar_tempMatrix_cp.tocsr()
				sipendar_tf_idf_matrix_birth_place_cp.append(sipendar_tempMatrix_cp)
				sipendar_WatchlistListID_birth_place_cp.append(pickle.loads(row[1]))
				if(len(sipendar_tf_idf_matrix_birth_place_cp) == 1):
					sipendar_vectorizer_birth_place_cp = pickle.loads(row[2])

				del sipendar_tempMatrix_cp

			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'Watchlist_Nationality_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_tempMatrix_cp = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_tempMatrix_cp):
					sipendar_tempMatrix_cp = sipendar_tempMatrix_cp.tocsr()
				sipendar_tf_idf_matrix_nationality_cp.append(sipendar_tempMatrix_cp)
				sipendar_WatchlistListID_nationality_cp.append(pickle.loads(row[1]))
				if(len(sipendar_tf_idf_matrix_nationality_cp) == 1):
					sipendar_vectorizer_nationality_cp = pickle.loads(row[2])

				del sipendar_tempMatrix_cp

			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'Watchlist_Identity_Number_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_tempMatrix_cp = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_tempMatrix_cp):
					sipendar_tempMatrix_cp = sipendar_tempMatrix_cp.tocsr()
				sipendar_tf_idf_matrix_identity_number_cp.append(sipendar_tempMatrix_cp)
				sipendar_WatchlistListID_identity_number_cp.append(pickle.loads(row[1]))
				if(len(sipendar_tf_idf_matrix_identity_number_cp) == 1):
					sipendar_vectorizer_identity_number_cp = pickle.loads(row[2])

				del sipendar_tempMatrix_cp

	del singleton_param

# Run load matrix sipendar customer prospect
#load_matrix_sipendar_prospect()	--14-Sep-2023 : Has been load on main function

class MatrixSipendarWatchlist(Resource):
	def get(self):
		global sipendar_tf_idf_matrix_nama_cp
		global sipendar_vectorizer_nama_cp
		global sipendar_WatchlistListID_nama_cp
		
		global sipendar_tf_idf_matrix_birth_place_cp
		global sipendar_vectorizer_birth_place_cp
		global sipendar_WatchlistListID_birth_place_cp

		global sipendar_tf_idf_matrix_nationality_cp
		global sipendar_vectorizer_nationality_cp
		global sipendar_WatchlistListID_nationality_cp

		global sipendar_tf_idf_matrix_identity_number_cp
		global sipendar_vectorizer_identity_number_cp
		global sipendar_WatchlistListID_identity_number_cp

		parser = reqparse.RequestParser()  # initialize
		parser.add_argument('fieldid', type=str)
		parser.add_argument('fieldidalias', type=str)
		parser.add_argument('fieldtocompare', type=str)
		parser.add_argument('fieldtocomparealias', type=str)
		parser.add_argument('table', type=str)
		args = parser.parse_args()  # parse arguments to dictionary

		fieldId = args['fieldid'].upper()
		fieldIdAlias = args['fieldidalias'].upper()
		fieldToCompare = args['fieldtocompare'].upper()
		fieldToCompareAlias = args['fieldtocomparealias'].upper()
		table = args['table'].upper()

		t1 = time.time()
		singleton_param = SingletonParam()
		with pyodbc.connect(singleton_param.connString) as _conn:

			names = pd.read_sql_query("SELECT " + fieldId + " AS " + fieldIdAlias + ", " + fieldToCompare + " AS " + fieldToCompareAlias + " FROM " + table + " WHERE ISNULL(" + fieldToCompare + ", '') <> '' ORDER BY " + fieldIdAlias, _conn)

			t = time.time()-t1
			print("SELECT SIPENDAR MATRIX DATA (Watchlist): " + fieldToCompareAlias + " " + str(t))
			t1 = time.time()

			if not names.empty:
				org_names = names[fieldToCompareAlias]

				vectorizer = TfidfVectorizer(min_df=1, analyzer="char_wb", ngram_range=(3,3))
				matrix = vectorizer.fit_transform(org_names)
				listID = names[fieldIdAlias]

				t = time.time()-t1
				print("GENERATE SIPENDAR MATRIX (Watchlist): " + fieldToCompareAlias + " " + str(t))
				t1 = time.time()

				with _conn.cursor() as _cursor:
					_cursor.execute("DELETE SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE ?", "Watchlist_" + fieldToCompareAlias + "_%")

				t = time.time()-t1
				print("DELETE SIPENDAR PREV DATA (Watchlist): " + fieldToCompareAlias + " " + str(t))
				t1 = time.time()

				dataSize = len(org_names)
				counter = 0
				counterIndex = 0

				vectorizerDump = pickle.dumps(vectorizer)

				if fieldToCompareAlias.upper() == "NAMA":
					sipendar_tf_idf_matrix_nama_cp = []
					sipendar_WatchlistListID_nama_cp = []
					sipendar_vectorizer_nama_cp = vectorizer
				elif fieldToCompareAlias.upper() == "BIRTH_PLACE":
					sipendar_tf_idf_matrix_birth_place_cp = []
					sipendar_WatchlistListID_birth_place_cp = []
					sipendar_vectorizer_birth_place_cp = vectorizer
				elif fieldToCompareAlias.upper() == "NATIONALITY":
					sipendar_tf_idf_matrix_nationality_cp = []
					sipendar_WatchlistListID_nationality_cp = []
					sipendar_vectorizer_nationality_cp = vectorizer
				elif fieldToCompareAlias.upper() == "IDENTITY_NUMBER":
					sipendar_tf_idf_matrix_identity_number_cp = []
					sipendar_WatchlistListID_identity_number_cp = []
					sipendar_vectorizer_identity_number_cp = vectorizer

				while counter < dataSize:
					t = time.time()-t1
					print("Processing data (Watchlist): " + str(counter) + " / " + str(dataSize) + " " + str(t))
					t1 = time.time()

					matrixSliced = matrix[counter: counter + singleton_param.pageSize].transpose()
					listIDSliced = listID[counter: counter + singleton_param.pageSize]

					t = time.time()-t1
					print("Slice Data (Watchlist): " + str(counter) + " / " + str(dataSize) + " " + str(t))
					t1 = time.time()

					matrixDump = pickle.dumps(matrixSliced)
					listIDDump = pickle.dumps(listIDSliced)

					with _conn.cursor() as _cursor:
						_cursor.execute("INSERT INTO SIPENDAR_CUSTOMER_MATRIX (MATRIX_NAME, MATRIX, VECTORIZER, LIST_ID) VALUES(?, ?, ?, ?)", ('Watchlist_' + fieldToCompareAlias + "_" + str(counterIndex + 1).zfill(2), pyodbc.Binary(matrixDump), pyodbc.Binary(vectorizerDump), pyodbc.Binary(listIDDump)))

					t = time.time()-t1
					print("Insert DB (Watchlist): " + str(counter) + " / " + str(dataSize) + " " + str(t))
					t1 = time.time()

					del matrixDump
					del listIDDump

					if fieldToCompareAlias.upper() == "NAMA":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_tf_idf_matrix_nama_cp.append(matrixSliced)
						sipendar_WatchlistListID_nama_cp.append(listIDSliced)
					elif fieldToCompareAlias.upper() == "BIRTH_PLACE":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_tf_idf_matrix_birth_place_cp.append(matrixSliced)
						sipendar_WatchlistListID_birth_place_cp.append(listIDSliced)
					elif fieldToCompareAlias.upper() == "NATIONALITY":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_tf_idf_matrix_nationality_cp.append(matrixSliced)
						sipendar_WatchlistListID_nationality_cp.append(listIDSliced)
					elif fieldToCompareAlias.upper() == "IDENTITY_NUMBER":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_tf_idf_matrix_identity_number_cp.append(matrixSliced)
						sipendar_WatchlistListID_identity_number_cp.append(listIDSliced)

					counter = counter + singleton_param.pageSize
					counterIndex = counterIndex + 1
			
					del matrixDump, listIDDump, matrixSliced, listIDSliced

				del org_names, vectorizer, matrix, listID, dataSize, counter, counterIndex, vectorizerDump

			del names
			
		del singleton_param

		t = time.time()-t1
		print("Done SIPENDAR Creating Matrix (Watchlist) : " + fieldToCompareAlias + " " + str(t))
		t1 = time.time()

		return "SUCCESS", 200  # return data and 200 OK
        
class CompareSipendarWatchlist():
	def DoCompare(tf_idf_matrix, matrix, listID, fieldToCompare, uniqID, names, offset):
		t1 = time.time()

		singleton_param = SingletonParam()

		matches = awesome_cossim_top(tf_idf_matrix, matrix, singleton_param.maximumRowMatch, singleton_param.minimumSimilarity, True, singleton_param.maximumCore)

		t = time.time()-t1
		print("COMPARE SIPENDAR Watchlist : " + fieldToCompare + " " + str(t))
		t1 = time.time()

		matches_df = get_matches_df(uniqID, matches, names, listID, offset)

		t = time.time()-t1
		print("MATCHING SIPENDAR Watchlist : " + fieldToCompare + " " + str(t))
		t1 = time.time()

		engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" % urllib.parse.quote_plus(singleton_param.connString), fast_executemany=True)

		matches_df.to_sql('SIPENDAR_SCREENING_RESULT_API', con=engine, if_exists='append', index=False)

		t = time.time()-t1
		print("INSERT SIPENDAR DB Watchlist : " + fieldToCompare + " " + str(t))
		t1 = time.time()

		del singleton_param, matches, matches_df, engine		

class ScreeningSipendarWatchlist(Resource):
	def get(self):
		parser = reqparse.RequestParser()  # initialize
		parser.add_argument('requestid', type=str)
		parser.add_argument('fieldtocompare', type=str)
		args = parser.parse_args()  # parse arguments to dictionary

		uniqID = str(uuid.uuid4())
		requestid = args['requestid']
		fieldToCompare = args['fieldtocompare'].upper()

		t1 = time.time()
		singleton_param = SingletonParam()
		with pyodbc.connect(singleton_param.connString) as _conn:

			names = pd.read_sql_query("SELECT PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID, " + fieldToCompare + " FROM SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL WHERE FK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_ID = " + requestid + " AND ISNULL(" + fieldToCompare + ", '') <> '' ORDER BY PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID", _conn)

			t = time.time()-t1
			print("SELECT SIPENDAR SCREENING (Watchlist): " + fieldToCompare + " " + str(t))
			t1 = time.time()

			if not names.empty:
				org_names = names[fieldToCompare]

				if fieldToCompare.upper() == "NAMA":
					matrix = sipendar_tf_idf_matrix_nama_cp
					vectorizer = sipendar_vectorizer_nama_cp
					listID = sipendar_WatchlistListID_nama_cp
				elif fieldToCompare.upper() == "BIRTH_PLACE":
					matrix = sipendar_tf_idf_matrix_birth_place_cp
					vectorizer = sipendar_vectorizer_birth_place_cp
					listID = sipendar_WatchlistListID_birth_place_cp
				elif fieldToCompare.upper() == "NATIONALITY":
					matrix = sipendar_tf_idf_matrix_nationality_cp
					vectorizer = sipendar_vectorizer_nationality_cp
					listID = sipendar_WatchlistListID_nationality_cp
				elif fieldToCompare.upper() == "IDENTITY_NUMBER":
					matrix = sipendar_tf_idf_matrix_identity_number_cp
					vectorizer = sipendar_vectorizer_identity_number_cp
					listID = sipendar_WatchlistListID_identity_number_cp

				tf_idf_matrix = vectorizer.transform(org_names)

				t = time.time()-t1
				print("CREATE SIPENDAR MATRIX (Watchlist) : " + fieldToCompare + " " + str(t))
				t1 = time.time()

				for i in range(len(matrix)):
					CompareSipendarWatchlist.DoCompare(tf_idf_matrix, matrix[i], listID[i], fieldToCompare, uniqID, names["PK_SIPENDAR_SCREENING_CUSTOMER_PROSPECT_REQUEST_DETAIL_ID"], i * singleton_param.pageSize)

			del matrix, vectorizer, listID, org_names, tf_idf_matrix

		t = time.time()-t1
		print("DONE SIPENDAR SCREENING (Watchlist) : " + fieldToCompare + " " + str(t))

		del args, singleton_param, names		

		return uniqID, 200  # return data and 200 OK        
################### End of 8 Nov 2021 : Tambahan untuk Customer Prospect

################### 09 November 2021 : Tambahan untuk Screening Stakeholder Delta (New Stakeholder Only) SiPendar
# 15-Nov-2022 Adi : Define function for loading SIPENDAR STAKEHOLDER DELTA Matrix so it can called from Load Balancer
def load_matrix_sipendar_stakeholder_delta():
	print("Load SIPENDAR STAKEHOLDER DELTA matrix...")

	global sipendar_stakeholder_delta_tf_idf_matrix_nama
	global sipendar_stakeholder_delta_vectorizer_nama
	global sipendar_stakeholder_delta_WatchlistListID_nama
	
	global sipendar_stakeholder_delta_tf_idf_matrix_birth_place
	global sipendar_stakeholder_delta_vectorizer_birth_place
	global sipendar_stakeholder_delta_WatchlistListID_birth_place

	global sipendar_stakeholder_delta_tf_idf_matrix_nationality
	global sipendar_stakeholder_delta_vectorizer_nationality
	global sipendar_stakeholder_delta_WatchlistListID_nationality

	global sipendar_stakeholder_delta_tf_idf_matrix_identity_number
	global sipendar_stakeholder_delta_vectorizer_identity_number
	global sipendar_stakeholder_delta_WatchlistListID_identity_number

	sipendar_stakeholder_delta_tf_idf_matrix_nama = []
	sipendar_stakeholder_delta_WatchlistListID_nama = []

	sipendar_stakeholder_delta_tf_idf_matrix_birth_place = []
	sipendar_stakeholder_delta_WatchlistListID_birth_place = []

	sipendar_stakeholder_delta_tf_idf_matrix_nationality = []
	sipendar_stakeholder_delta_WatchlistListID_nationality = []

	sipendar_stakeholder_delta_tf_idf_matrix_identity_number = []
	sipendar_stakeholder_delta_WatchlistListID_identity_number = []	

	singleton_param = SingletonParam()
	with pyodbc.connect(singleton_param.connString) as _conn:
		with _conn.cursor() as _cursor:
			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'NewStakeholder_Nama_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_stakeholder_delta_tempMatrix = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_stakeholder_delta_tempMatrix):
					sipendar_stakeholder_delta_tempMatrix = sipendar_stakeholder_delta_tempMatrix.tocsr()
				sipendar_stakeholder_delta_tf_idf_matrix_nama.append(sipendar_stakeholder_delta_tempMatrix)
				sipendar_stakeholder_delta_WatchlistListID_nama.append(pickle.loads(row[1]))
				if(len(sipendar_stakeholder_delta_tf_idf_matrix_nama) == 1):
					sipendar_stakeholder_delta_vectorizer_nama = pickle.loads(row[2])

				del sipendar_stakeholder_delta_tempMatrix

			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'NewStakeholder_Birth_Place_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_stakeholder_delta_tempMatrix = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_stakeholder_delta_tempMatrix):
					sipendar_stakeholder_delta_tempMatrix = sipendar_stakeholder_delta_tempMatrix.tocsr()
				sipendar_stakeholder_delta_tf_idf_matrix_birth_place.append(sipendar_stakeholder_delta_tempMatrix)
				sipendar_stakeholder_delta_WatchlistListID_birth_place.append(pickle.loads(row[1]))
				if(len(sipendar_stakeholder_delta_tf_idf_matrix_birth_place) == 1):
					sipendar_stakeholder_delta_vectorizer_birth_place = pickle.loads(row[2])

				del sipendar_stakeholder_delta_tempMatrix

			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'NewStakeholder_Nationality_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_stakeholder_delta_tempMatrix = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_stakeholder_delta_tempMatrix):
					sipendar_stakeholder_delta_tempMatrix = sipendar_stakeholder_delta_tempMatrix.tocsr()
				sipendar_stakeholder_delta_tf_idf_matrix_nationality.append(sipendar_stakeholder_delta_tempMatrix)
				sipendar_stakeholder_delta_WatchlistListID_nationality.append(pickle.loads(row[1]))
				if(len(sipendar_stakeholder_delta_tf_idf_matrix_nationality) == 1):
					sipendar_stakeholder_delta_vectorizer_nationality = pickle.loads(row[2])

				del sipendar_stakeholder_delta_tempMatrix

			_cursor.execute("SELECT MATRIX, LIST_ID, VECTORIZER FROM SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE 'NewStakeholder_Identity_Number_%' ORDER BY MATRIX_NAME")
			for row in _cursor.fetchall():
				sipendar_stakeholder_delta_tempMatrix = pickle.loads(row[0])
				if not isspmatrix_csr(sipendar_stakeholder_delta_tempMatrix):
					sipendar_stakeholder_delta_tempMatrix = sipendar_stakeholder_delta_tempMatrix.tocsr()
				sipendar_stakeholder_delta_tf_idf_matrix_identity_number.append(sipendar_stakeholder_delta_tempMatrix)
				sipendar_stakeholder_delta_WatchlistListID_identity_number.append(pickle.loads(row[1]))
				if(len(sipendar_stakeholder_delta_tf_idf_matrix_identity_number) == 1):
					sipendar_stakeholder_delta_vectorizer_identity_number = pickle.loads(row[2])

				del sipendar_stakeholder_delta_tempMatrix

	del singleton_param

# Run load matrix sipendar stakeholder delta
#load_matrix_sipendar_stakeholder_delta()		--14-Sep-2023 : Has been load on main function

class MatrixSipendarStakeholderDelta(Resource):
	def get(self):
		global sipendar_stakeholder_delta_tf_idf_matrix_nama
		global sipendar_stakeholder_delta_vectorizer_nama
		global sipendar_stakeholder_delta_WatchlistListID_nama
		
		global sipendar_stakeholder_delta_tf_idf_matrix_birth_place
		global sipendar_stakeholder_delta_vectorizer_birth_place
		global sipendar_stakeholder_delta_WatchlistListID_birth_place

		global sipendar_stakeholder_delta_tf_idf_matrix_nationality
		global sipendar_stakeholder_delta_vectorizer_nationality
		global sipendar_stakeholder_delta_WatchlistListID_nationality

		global sipendar_stakeholder_delta_tf_idf_matrix_identity_number
		global sipendar_stakeholder_delta_vectorizer_identity_number
		global sipendar_stakeholder_delta_WatchlistListID_identity_number

		parser = reqparse.RequestParser()  # initialize
		parser.add_argument('fieldid', type=str)
		parser.add_argument('fieldidalias', type=str)
		parser.add_argument('fieldtocompare', type=str)
		parser.add_argument('fieldtocomparealias', type=str)
		parser.add_argument('table', type=str)
		args = parser.parse_args()  # parse arguments to dictionary

		fieldId = args['fieldid'].upper()
		fieldIdAlias = args['fieldidalias'].upper()
		fieldToCompare = args['fieldtocompare'].upper()
		fieldToCompareAlias = args['fieldtocomparealias'].upper()
		table = args['table'].upper()

		t1 = time.time()
		singleton_param = SingletonParam()
		with pyodbc.connect(singleton_param.connString) as _conn:

			names = pd.read_sql_query("SELECT " + fieldId + " AS " + fieldIdAlias + ", " + fieldToCompare + " AS " + fieldToCompareAlias + " FROM " + table + " WHERE ISNULL(" + fieldToCompare + ", '') <> '' ORDER BY " + fieldIdAlias, _conn)

			t = time.time()-t1
			print("SELECT SIPENDAR MATRIX DATA (STAKEHOLDER DELTA): " + fieldToCompareAlias + " " + str(t))
			t1 = time.time()

			if not names.empty:
				org_names = names[fieldToCompareAlias]

				vectorizer = TfidfVectorizer(min_df=1, analyzer="char_wb", ngram_range=(3,3))
				matrix = vectorizer.fit_transform(org_names)
				listID = names[fieldIdAlias]

				t = time.time()-t1
				print("GENERATE SIPENDAR MATRIX (STAKEHOLDER DELTA): " + fieldToCompareAlias + " " + str(t))
				t1 = time.time()

				with _conn.cursor() as _cursor:
					_cursor.execute("DELETE SIPENDAR_CUSTOMER_MATRIX WHERE MATRIX_NAME LIKE ?", "NewStakeholder_" + fieldToCompareAlias + "_%")

				t = time.time()-t1
				print("DELETE SIPENDAR PREV DATA (STAKEHOLDER DELTA): " + fieldToCompareAlias + " " + str(t))
				t1 = time.time()

				dataSize = len(org_names)
				counter = 0
				counterIndex = 0

				vectorizerDump = pickle.dumps(vectorizer)

				if fieldToCompareAlias.upper() == "NAMA":
					sipendar_stakeholder_delta_tf_idf_matrix_nama = []
					sipendar_stakeholder_delta_WatchlistListID_nama = []
					sipendar_stakeholder_delta_vectorizer_nama = vectorizer
				elif fieldToCompareAlias.upper() == "BIRTH_PLACE":
					sipendar_stakeholder_delta_tf_idf_matrix_birth_place = []
					sipendar_stakeholder_delta_WatchlistListID_birth_place = []
					sipendar_stakeholder_delta_vectorizer_birth_place = vectorizer
				elif fieldToCompareAlias.upper() == "NATIONALITY":
					sipendar_stakeholder_delta_tf_idf_matrix_nationality = []
					sipendar_stakeholder_delta_WatchlistListID_nationality = []
					sipendar_stakeholder_delta_vectorizer_nationality = vectorizer
				elif fieldToCompareAlias.upper() == "IDENTITY_NUMBER":
					sipendar_stakeholder_delta_tf_idf_matrix_identity_number = []
					sipendar_stakeholder_delta_WatchlistListID_identity_number = []
					sipendar_stakeholder_delta_vectorizer_identity_number = vectorizer

				while counter < dataSize:
					t = time.time()-t1
					print("Processing data  (STAKEHOLDER DELTA): " + str(counter) + " / " + str(dataSize) + " " + str(t))
					t1 = time.time()

					matrixSliced = matrix[counter: counter + singleton_param.pageSize].transpose()
					listIDSliced = listID[counter: counter + singleton_param.pageSize]

					t = time.time()-t1
					print("Slice Data  (STAKEHOLDER DELTA): " + str(counter) + " / " + str(dataSize) + " " + str(t))
					t1 = time.time()

					matrixDump = pickle.dumps(matrixSliced)
					listIDDump = pickle.dumps(listIDSliced)

					with _conn.cursor() as _cursor:
						_cursor.execute("INSERT INTO SIPENDAR_CUSTOMER_MATRIX (MATRIX_NAME, MATRIX, VECTORIZER, LIST_ID) VALUES(?, ?, ?, ?)", ('NewStakeholder_' + fieldToCompareAlias + "_" + str(counterIndex + 1).zfill(2), pyodbc.Binary(matrixDump), pyodbc.Binary(vectorizerDump), pyodbc.Binary(listIDDump)))

					t = time.time()-t1
					print("Insert DB  (STAKEHOLDER DELTA): " + str(counter) + " / " + str(dataSize) + " " + str(t))
					t1 = time.time()

					del matrixDump
					del listIDDump

					if fieldToCompareAlias.upper() == "NAMA":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_stakeholder_delta_tf_idf_matrix_nama.append(matrixSliced)
						sipendar_stakeholder_delta_WatchlistListID_nama.append(listIDSliced)
					elif fieldToCompareAlias.upper() == "BIRTH_PLACE":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_stakeholder_delta_tf_idf_matrix_birth_place.append(matrixSliced)
						sipendar_stakeholder_delta_WatchlistListID_birth_place.append(listIDSliced)
					elif fieldToCompareAlias.upper() == "NATIONALITY":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_stakeholder_delta_tf_idf_matrix_nationality.append(matrixSliced)
						sipendar_stakeholder_delta_WatchlistListID_nationality.append(listIDSliced)
					elif fieldToCompareAlias.upper() == "IDENTITY_NUMBER":
						if not isspmatrix_csr(matrixSliced):
							matrixSliced = matrixSliced.tocsr()
						sipendar_stakeholder_delta_tf_idf_matrix_identity_number.append(matrixSliced)
						sipendar_stakeholder_delta_WatchlistListID_identity_number.append(listIDSliced)

					counter = counter + singleton_param.pageSize
					counterIndex = counterIndex + 1
		
					del matrixDump, listIDDump, matrixSliced, listIDSliced

				del org_names, vectorizer, matrix, listID, dataSize, counter, counterIndex, vectorizerDump

			del names
			
		del singleton_param

		t = time.time()-t1
		print("Done SIPENDAR Creating Matrix  (STAKEHOLDER DELTA): " + fieldToCompareAlias + " " + str(t))
		t1 = time.time()

		return "SUCCESS", 200  # return data and 200 OK

class CompareSipendarStakeholderDelta():
	def DoCompare(tf_idf_matrix, matrix, listID, fieldToCompare, uniqID, names, offset):
		t1 = time.time()

		singleton_param = SingletonParam()

		matches = awesome_cossim_top(tf_idf_matrix, matrix,singleton_param. maximumRowMatch, singleton_param.minimumSimilarity, True, singleton_param.maximumCore)

		t = time.time()-t1
		print("COMPARE SIPENDAR  (STAKEHOLDER DELTA): " + fieldToCompare + " " + str(t))
		t1 = time.time()

		matches_df = get_matches_df(uniqID, matches, names, listID, offset)

		t = time.time()-t1
		print("MATCHING SIPENDAR  (STAKEHOLDER DELTA): " + fieldToCompare + " " + str(t))
		t1 = time.time()

		engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" % urllib.parse.quote_plus(singleton_param.connString), fast_executemany=True)

		matches_df.to_sql('SIPENDAR_SCREENING_RESULT_API', con=engine, if_exists='append', index=False)

		t = time.time()-t1
		print("INSERT SIPENDAR DB (STAKEHOLDER DELTA): " + fieldToCompare + " " + str(t))
		t1 = time.time()

		del singleton_param, matches, matches_df, engine		

class ScreeningSipendarStakeholderDelta(Resource):
	def get(self):
		parser = reqparse.RequestParser()  # initialize
		parser.add_argument('requestid', type=str)
		parser.add_argument('fieldtocompare', type=str)
		args = parser.parse_args()  # parse arguments to dictionary

		uniqID = str(uuid.uuid4())
		requestid = args['requestid']
		fieldToCompare = args['fieldtocompare'].upper()

		t1 = time.time()
		singleton_param = SingletonParam()
		with pyodbc.connect(singleton_param.connString) as _conn:

			names = pd.read_sql_query("SELECT PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID, " + fieldToCompare + " FROM SIPENDAR_SCREENING_REQUEST_DETAIL WHERE FK_SIPENDAR_SCREENING_REQUEST_ID = " + requestid + " AND ISNULL(" + fieldToCompare + ", '') <> '' ORDER BY PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID", _conn)

			t = time.time()-t1
			print("SELECT SIPENDAR SCREENING (STAKEHOLDER DELTA): " + fieldToCompare + " " + str(t))
			t1 = time.time()

			if not names.empty:
				org_names = names[fieldToCompare]

				if fieldToCompare.upper() == "NAMA":
					matrix = sipendar_stakeholder_delta_tf_idf_matrix_nama
					vectorizer = sipendar_stakeholder_delta_vectorizer_nama
					listID = sipendar_stakeholder_delta_WatchlistListID_nama
				elif fieldToCompare.upper() == "BIRTH_PLACE":
					matrix = sipendar_stakeholder_delta_tf_idf_matrix_birth_place
					vectorizer = sipendar_stakeholder_delta_vectorizer_birth_place
					listID = sipendar_stakeholder_delta_WatchlistListID_birth_place
				elif fieldToCompare.upper() == "NATIONALITY":
					matrix = sipendar_stakeholder_delta_tf_idf_matrix_nationality
					vectorizer = sipendar_delta_vectorizer_nationality
					listID = sipendar_stakeholder_delta_WatchlistListID_nationality
				elif fieldToCompare.upper() == "IDENTITY_NUMBER":
					matrix = sipendar_stakeholder_delta_tf_idf_matrix_identity_number
					vectorizer = sipendar_stakeholder_delta_vectorizer_identity_number
					listID = sipendar_stakeholder_delta_WatchlistListID_identity_number

				tf_idf_matrix = vectorizer.transform(org_names)

				t = time.time()-t1
				print("CREATE SIPENDAR MATRIX (STAKEHOLDER DELTA): " + fieldToCompare + " " + str(t))
				t1 = time.time()

				for i in range(len(matrix)):
					CompareSipendarStakeholderDelta.DoCompare(tf_idf_matrix, matrix[i], listID[i], fieldToCompare, uniqID, names["PK_SIPENDAR_SCREENING_REQUEST_DETAIL_ID"], i * singleton_param.pageSize)

			del matrix, vectorizer, listID, org_names, tf_idf_matrix

		t = time.time()-t1
		print("DONE SIPENDAR SCREENING (STAKEHOLDER DELTA) : " + fieldToCompare + " " + str(t))

		del args, singleton_param, names		

		return uniqID, 200  # return data and 200 OK
################### End of 09 Nov 2021 : Tambahan untuk Screening Stakeholder Delta (New Stakeholder Only) SiPendar
############################################ SIPENDAR #################################################################


################### 20 Sep 2021 : Case Management AI (STR/Not STR Prediction)
import joblib
from datetime import date
from sklearn import linear_model

today = date.today()
today=pd.to_datetime(today)

def TransposeCategory(dataset, key):
    res = dataset[key].str.get_dummies()
    dataset = dataset.drop([key], axis=1)
    for k in list(res):
        res[key + "_" + k] = res[k]
        res = res.drop([k], axis=1)
    return dataset.join(res)

class CaseManagementAI(Resource):
	def get(self):
		print("Process Case Management AI Prediction started...")
		t2 = time.time()

		# Get Data from Database for Modeling
		t1 = time.time()
		singleton_param =SingletonParam()
		with pyodbc.connect(singleton_param.connString) as _conn:
			query = "SELECT * FROM OneFCC_Case_Management_AI_Model"
			mainDataset = pd.read_sql_query(query, _conn)

			t = time.time() - t1
			print("Load data from Database to build model done in " + str(t) + " seconds.")

			IsDataModelEmpty = 0

			if mainDataset.empty:
				print('Dataframe is empty!')
				IsDataModelEmpty = 1
			else:
				# Fill Status NULL with 9999
				mainDataset['FK_LastProposedStatusID'] = mainDataset['FK_LastProposedStatusID'].fillna(9999)
				mainDataset.FK_LastProposedStatusID = mainDataset.FK_LastProposedStatusID.astype(int)

				# Save status not in (3,4) as Test Dataset. In the end it will used to insert to Result Table.
				testDataset = mainDataset['PK_OneFCC_Case_Management_AI_Model_ID'].loc[~mainDataset.FK_LastProposedStatusID.isin([3,4])]

				# Default not STR if status not in (3,4)
				mainDataset.loc[~mainDataset.FK_LastProposedStatusID.isin([3,4]), 'FK_LastProposedStatusID'] = 0
				# Status 3 is not STR
				mainDataset.loc[mainDataset.FK_LastProposedStatusID == 3, 'FK_LastProposedStatusID'] = 0
				# Status 4 is STR
				mainDataset.loc[mainDataset.FK_LastProposedStatusID == 4, 'FK_LastProposedStatusID'] = 1		

				# Prepare for Result Dataset
				resultDataset = mainDataset

				# Build the data model
				t1 = time.time()
				modelDataset = mainDataset

				#Drop field yang tidak digunakan untuk model (seperti PK dan FK)
				modelDataset = modelDataset.drop(['PK_OneFCC_Case_Management_AI_Model_ID', 'FK_CaseManagement_ID'], axis=1)

				#Cleansing Data NULL
				today = date.today()
				today=pd.to_datetime(today)

				modelDataset['OpeningDate'] = modelDataset['OpeningDate'].fillna(today)
				modelDataset['DateOfBirth'] = modelDataset['DateOfBirth'].fillna(today)

				modelDataset['ModusDebet'] = modelDataset['ModusDebet'].fillna(0)
				modelDataset['StandarDeviasiDebet'] = modelDataset['StandarDeviasiDebet'].fillna(0)
				modelDataset['ModusCredit'] = modelDataset['ModusCredit'].fillna(0)
				modelDataset['StandarDeviasiCredit'] = modelDataset['StandarDeviasiCredit'].fillna(0)
				modelDataset['OutlierDebet'] = modelDataset['OutlierDebet'].fillna(0)
				modelDataset['OutlierCredit'] = modelDataset['OutlierCredit'].fillna(0)
				modelDataset['StandarDeviasiDebet'] = modelDataset['StandarDeviasiDebet'].fillna(0)
				modelDataset['StandarDeviasiDebet'] = modelDataset['StandarDeviasiDebet'].fillna(0)
				modelDataset['Segment'] = modelDataset['Segment'].fillna(0)
				modelDataset['SBU'] = modelDataset['SBU'].fillna(0)
				modelDataset['SUBSBU'] = modelDataset['SUBSBU'].fillna(0)
				modelDataset['TierAmountCredit'] = modelDataset['TierAmountCredit'].fillna(0)
				modelDataset['TierAmountDebit'] = modelDataset['TierAmountDebit'].fillna(0)
				modelDataset['FrqCredit'] = modelDataset['FrqCredit'].fillna(0)
				modelDataset['FrqDebit'] = modelDataset['FrqDebit'].fillna(0)
				modelDataset['PBK'] = modelDataset['PBK'].fillna(0)
				modelDataset['TARIK'] = modelDataset['TARIK'].fillna(0)
				modelDataset['SETOR'] = modelDataset['SETOR'].fillna(0)
				modelDataset['TMLN'] = modelDataset['TMLN'].fillna(0)
				modelDataset['TKLN'] = modelDataset['TKLN'].fillna(0)
				modelDataset['TMDN'] = modelDataset['TMDN'].fillna(0)
				modelDataset['TKDN'] = modelDataset['TKDN'].fillna(0)
				modelDataset['KRED'] = modelDataset['KRED'].fillna(0)
				modelDataset['INTER'] = modelDataset['INTER'].fillna(0)

				# Transpose Category
				modelDataset = TransposeCategory(modelDataset, "CaseDescription")
				modelDataset = TransposeCategory(modelDataset, "CustomerType")
				modelDataset = TransposeCategory(modelDataset, "CustomerSubClass")
				modelDataset = TransposeCategory(modelDataset, "Citizenship")
				modelDataset = TransposeCategory(modelDataset, "BusinessType")
				modelDataset = TransposeCategory(modelDataset, "IndustryCode")
				modelDataset = TransposeCategory(modelDataset, "BirthPlace")
				modelDataset = TransposeCategory(modelDataset, "Gender")
				modelDataset.Segment = modelDataset.Segment.astype(str)
				modelDataset = TransposeCategory(modelDataset, "Segment")
				modelDataset.SBU = modelDataset.SBU.astype(str)
				modelDataset = TransposeCategory(modelDataset, "SBU")
				modelDataset.SUBSBU = modelDataset.SUBSBU.astype(str)
				modelDataset = TransposeCategory(modelDataset, "SUBSBU")
				modelDataset.TierAmountCredit = modelDataset.TierAmountCredit.astype(str)
				modelDataset = TransposeCategory(modelDataset, "TierAmountCredit")
				modelDataset.TierAmountDebit = modelDataset.TierAmountDebit.astype(str)
				modelDataset = TransposeCategory(modelDataset, "TierAmountDebit")		

				# Calculate Account Age and Customer Age
				modelDataset['AccountAge'] = (today - modelDataset['OpeningDate'])/np.timedelta64(1,'D')
				modelDataset['CustomerAge'] = (today - modelDataset['DateOfBirth'])/np.timedelta64(1,'Y')
				modelDataset = modelDataset.drop(['OpeningDate', 'DateOfBirth'], axis=1)	

				# Get the x and y
				y = modelDataset['FK_LastProposedStatusID']
				X = modelDataset[filter(lambda x: x != "FK_LastProposedStatusID", list(modelDataset))] 	

				# New Data for Testing. Samakan saja jumlah test data dengan model data agar indexnya sesuai.
				X_new = X

				# Create a logistic regression model logreg and fit it to the data.
				logreg = linear_model.LogisticRegression()
				logreg.fit(X,y)		

				t = time.time() - t1
				print("Build data model done in " + str(t) + " seconds.")


				# Calculate Probability
				t1 = time.time()
				grid_new = logreg.predict(X_new)
				grid_probability_new = logreg.predict_proba(X_new)		

				resultDataset = resultDataset.drop(['CaseDescription', 'OpeningDate', 'CustomerType', 'CustomerSubClass', 'Citizenship', 'BusinessType', 'IndustryCode', 'BirthPlace', 'DateOfBirth', 'Gender', 'ModusDebet', 'StandarDeviasiDebet', 'ModusCredit', 'StandarDeviasiCredit', 'OutlierDebet', 'OutlierCredit', 'FK_LastProposedStatusID', 'Segment', 'SBU', 'SUBSBU', 'TierAmountCredit', 'TierAmountDebit', 'FrqCredit', 'FrqDebit', 'PBK', 'TARIK', 'SETOR', 'TMLN', 'TKLN', 'TMDN', 'TKDN', 'KRED', 'INTER'], axis=1)
				resultDataset["PredictStatus"] = grid_new		

				resultDataset = resultDataset[(resultDataset.PredictStatus == 0) | (resultDataset.PredictStatus == 1)]
				resultDataset.loc[resultDataset.PredictStatus == 0, 'PredictStatus'] = 3
				resultDataset.loc[resultDataset.PredictStatus == 1, 'PredictStatus'] = 4
				resultDataset.PredictStatus = resultDataset.PredictStatus.astype(int)

				resultDataset["Probability3"] = grid_probability_new[:,0:1]
				resultDataset["Probability4"] = grid_probability_new[:,1:2]

				resultDataset["Probability3"] = round(resultDataset["Probability3"],2)
				resultDataset["Probability4"] = round(resultDataset["Probability4"],2)

				# Get Result only with beginning Status not in (3,4)
				resultDataset = resultDataset[resultDataset.index.isin(testDataset.index)]			

				t = time.time() - t1
				print("Calculate Probabilities done in " + str(t) + " seconds.")


				# Insert Results to Database
				t1 = time.time()
				import datetime

				#Create Table Temp
				with _conn.cursor() as _cursor:
					strQuery = "DROP TABLE IF EXISTS OneFCC_Case_Management_AI_Result_Temp"
					strQuery += " SELECT TOP 0 * INTO OneFCC_Case_Management_AI_Result_Temp FROM OneFCC_Case_Management_AI_Result WITH (NOLOCK)"
					_cursor.execute(strQuery)

					for index, row in resultDataset.iterrows():
						_cursor.execute("INSERT INTO OneFCC_Case_Management_AI_Result_Temp (FK_OneFCC_Case_Management_AI_Model_ID,FK_CaseManagement_ID,PredictStatus,Probability3,Probability4,CreatedDate) values(?,?,?,?,?,?)", row.PK_OneFCC_Case_Management_AI_Model_ID,row.FK_CaseManagement_ID,row.PredictStatus,row.Probability3,row.Probability4,datetime.datetime.now())

					#Update if data exists
					strQuery = "UPDATE dst SET"
					strQuery += " PredictStatus=src.PredictStatus,"
					strQuery += " Probability3=src.Probability3,"
					strQuery += " Probability4=src.Probability4,"
					strQuery += " LastUpdateDate=GETDATE()"			
					strQuery += " FROM OneFCC_Case_Management_AI_Result_Temp src"
					strQuery += " JOIN OneFCC_Case_Management_AI_Result dst"
					strQuery += " ON src.FK_CaseManagement_ID = dst.FK_CaseManagement_ID"
					_cursor.execute(strQuery)

					#Insert if data not exists
					strQuery = "INSERT INTO OneFCC_Case_Management_AI_Result ("
					strQuery += " FK_OneFCC_Case_Management_AI_Model_ID,"
					strQuery += " FK_CaseManagement_ID,"
					strQuery += " PredictStatus,"
					strQuery += " Probability3,"
					strQuery += " Probability4,"
					strQuery += " CreatedDate)"
					strQuery += " SELECT"
					strQuery += " src.FK_OneFCC_Case_Management_AI_Model_ID,"
					strQuery += " src.FK_CaseManagement_ID,"
					strQuery += " src.PredictStatus,"
					strQuery += " src.Probability3,"
					strQuery += " src.Probability4,"
					strQuery += " GETDATE()"
					strQuery += " FROM OneFCC_Case_Management_AI_Result_Temp src"
					strQuery += " LEFT JOIN OneFCC_Case_Management_AI_Result dst"
					strQuery += " ON src.FK_CaseManagement_ID = dst.FK_CaseManagement_ID"
					strQuery += " WHERE dst.PK_OneFCC_Case_Management_AI_Result_ID IS NULL"
					_cursor.execute(strQuery)

				del modelDataset, resultDataset

				t = time.time() - t1
				print("Insert Results to Database done in " + str(t) + " seconds.")			

				t = time.time() - t2
				print("Process Case Management AI Prediction done in " + str(t) + " seconds.")
			
			del mainDataset
	
		if IsDataModelEmpty==0:
			return "SUCCESS in " + str(t) + "seconds.", 200  # return data and 200 OK
		else:
			return "FAILED. Data Model is Empty!", 500
################### End of 20 Sep 2021 : Case Management AI		


### For testing API
class Test(Resource):
	def get(self):
		singleton_param = SingletonParam()
		_portPython = singleton_param.portPython
		del singleton_param

		return "Python API is Running on Port : " + str(_portPython), 200  # return data and 200 OK

api.add_resource(Test, '/Test')  # add endpoints
api.add_resource(Screening, '/Screening')  # add endpoints
api.add_resource(Matrix, '/Matrix')  # add endpoints
api.add_resource(ScreeningAPI, '/ScreeningAPI')  # add endpoints

########## SIPENDAR End Points ##########
api.add_resource(ScreeningSipendar, '/ScreeningSipendar')  # add endpoints
api.add_resource(MatrixSipendar, '/MatrixSipendar')  # add endpoints
api.add_resource(ScreeningSipendarDelta, '/ScreeningSipendarDelta')  # add endpoints
api.add_resource(MatrixSipendarDelta, '/MatrixSipendarDelta')  # add endpoints
api.add_resource(ScreeningSipendarStakeholder, '/ScreeningSipendarStakeholder')  # add endpoints
api.add_resource(MatrixSipendarStakeholder, '/MatrixSipendarStakeholder')  # add endpoints
api.add_resource(ScreeningSipendarWatchlist, '/ScreeningSipendarWatchlist')  # add endpoints
api.add_resource(MatrixSipendarWatchlist, '/MatrixSipendarWatchlist')  # add endpoints
api.add_resource(ScreeningSipendarStakeholderDelta, '/ScreeningSipendarStakeholderDelta')  # add endpoints
api.add_resource(MatrixSipendarStakeholderDelta, '/MatrixSipendarStakeholderDelta')  # add endpoints
########## END OF SIPENDAR End Points ##########

#15-Nov-2022 Adi : Tambah End Point untuk Load Matrix
api.add_resource(LoadMatrix, '/LoadMatrix')  # add endpoints
#16-Nov-2022 Adi : Tambah End Point untuk Generate Matrix
api.add_resource(GenerateMatrix, '/GenerateMatrix')  # add endpoints
#03-Jan-2023 Adi : Tambah End Point Screening Batch by Customer Matrix
api.add_resource(ScreeningBatchByCustomerMatrix, '/ScreeningBatchByCustomerMatrix')  # add endpoints
#11-Jan-2023 Adi : Tambah End Point Case Management AI
api.add_resource(CaseManagementAI, '/CaseManagementAI')  # add endpoints
#03-Jan-2023 Adi : Tambah End Point Screening Batch Delta by Customer Matrix
api.add_resource(ScreeningBatchDeltaByCustomerMatrix, '/ScreeningBatchDeltaByCustomerMatrix')  # add endpoints


if __name__ == '__main__':
	#Loading Parameters
	singleton_param = SingletonParam()
	singleton_param.load_match_score_parameter()
	singleton_param.load_parameter_write_log()

	#Assign Lates Version
	singleton_param.version = "2.2.2 - ReCode using Singleton Pattern and Memory Management"

	#Assing Port if you want to use Load Balancer (Comment it if you want using value from application parameter)
	#singleton_param.portPython = 1501

	#application/system parameters
	print("Load Application parameters:")
	print("Maximum Core used: " + str(singleton_param.maximumCore))
	print("Maximum Rows Matched returned: " + str(singleton_param.maximumRowMatch))
	print("Minimum Similarity(%) returned: " + str(singleton_param.minimumSimilarity*100))
	print("Python Port: " + str(singleton_param.portPython))
	print("Matrix Page Size: " + str(singleton_param.pageSize))
	print("Name Weight (%): " + str(singleton_param.nameWeight*100))
	print("DOB Weight (%): " + str(singleton_param.dobWeight*100))
	print("Nationality Weight (%): " + str(singleton_param.nationalityWeight*100))
	print("Identity Weight (%): " + str(singleton_param.identityWeight*100))
	print("Minimum Match Score Percentage (%): " + str(singleton_param.minReturnPercentage*100))

	#9-May-2023 Adi : Tambah Log ke EODLogSP (ON/OFF)
	strYesNo = "No"
	if singleton_param.isWriteLog == 1:
		strYesNo = "Yes"
	print("CDD Python Screening Write Log : " + strYesNo)

	# Load Matrix ALL (AML, SIPENDAR)
	load_matrix_all()

	#get time elapsed load API
	t_elapsed = time.time() - t_awal
	print("API READY in " + str(t_elapsed) + " seconds. Port : " + str(singleton_param.portPython))

	with pyodbc.connect(singleton_param.connString) as _conn:
		with _conn.cursor() as _cursor:
			_cursor.execute("INSERT INTO LogConsoleService (LogCreatedDate, LogStatus, LogInfo, LogDescription) VALUES(getdate(), 'INFO', ?, ?)", ('Python API Started', 'API READY on PORT: ' + str(singleton_param.portPython) + ' Version: ' + strVersion))

			# v2.2.1 : 14-Jun-2023 Adi : Tambah Instances Number Successfully Loaded for Load Balancer
			_cursor.execute("UPDATE SystemParameter SET SettingValue=TRY_CONVERT(INT, SettingValue)+1 WHERE PK_SystemParameter_ID=8027")

	from waitress import serve
	serve(app, host="0.0.0.0", port=singleton_param.portPython,)

	del singleton_param
