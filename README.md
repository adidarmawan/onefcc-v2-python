# **Change Log - NawaData OneFCC v2 Python Code**

This log was last generated on Mon, 2 Oct 2023 10:13:00 GMT+7.

# 2.2.3
Mon, 2 Oct 2023 10:13:00 GMT+7

**Changes**
1. Fixing some bugs and performance issues on ScreeningAPI end point

# 2.2.3 (Beta)
Tue, 26 Sep 2023 20:10:00 GMT+7

**Changes**
1. Apply singleton pattern for AML Matrix.
2. Fixing wrong usage of singleton pattern on previous version.

# 2.2.2
Mon, 18 Sept 2023 17:13:00 GMT+7

**Changes**
1. Apply singleton pattern for some parameters (except for matrix and vector).
2. Call DB Connection and Cursor inside "with statement".
3. Delete local variables when no longer needed to free up memory.